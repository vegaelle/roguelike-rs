use specs::prelude::*;
use super::{SufferDamage, World, Pools, Player, Name, RunState, Position, Map,
    Equipped, InBackpack, LootTable};
use super::gamelog::GameLog;

pub struct DamageSystem {}

impl<'a> System<'a> for DamageSystem {
    type SystemData = ( WriteStorage<'a, Pools>,
                        WriteStorage<'a, SufferDamage>,
                        ReadStorage<'a, Position>,
                        WriteExpect<'a, Map>,
                        Entities<'a> );

    fn run(&mut self, data: Self::SystemData) {
        let (mut stats, mut damage, positions, mut map, entities) = data;

        for (entity, mut stats, damage) in (&entities, &mut stats, &mut damage).join() {
            stats.hit_points.current -= damage.amount.iter().sum::<i32>();
            let pos = positions.get(entity);
            if let Some(pos) = pos {
                let idx = map.xy_idx(pos.x, pos.y);
                map.bloodstains.insert(idx);
            }
        }
        damage.clear();
    }

}

impl DamageSystem {

    pub fn delete_the_dead(ecs: &mut World) {
        let mut dead: Vec<Entity> = Vec::new();
        // Using a scope to make the borrow checker happy
        {
            let pools = ecs.read_storage::<Pools>();
            let players = ecs.read_storage::<Player>();
            let names = ecs.read_storage::<Name>();
            let entities = ecs.entities();
            let mut log = ecs.write_resource::<GameLog>();
            for (entity, stats) in (&entities, &pools).join() {
                if stats.hit_points.current < 1 {
                    let player = players.get(entity);
                    match player {
                        None => {
                            let victim_name = names.get(entity);
                            if let Some(victim_name) = victim_name {
                                log.entries.push(format!("{} is dead", &victim_name.name));
                            }
                            dead.push(entity)
                        }
                        Some(_) => {
                            // log.entries.push("You are dead".to_string())
                            let mut runstate = ecs.write_resource::<RunState>();
                            *runstate = RunState::GameOver;
                        }
                    }
                }
            }
        }
        // Drop everything held by dead people
        let mut to_spawn: Vec<(String, Position)> = Vec::new();
        {
            let mut to_drop: Vec<(Entity, Position)> = Vec::new();
            let entities = ecs.entities();
            let mut equipped = ecs.write_storage::<Equipped>();
            let mut carried = ecs.write_storage::<InBackpack>();
            let mut positions = ecs.write_storage::<Position>();
            let loot_tables = ecs.read_storage::<LootTable>();
            let mut rng = ecs.write_resource::<rltk::RandomNumberGenerator>();
            for victim in dead.iter() {
                let pos = positions.get(*victim);
                for (entity, equipped) in (&entities, &equipped).join() {
                    if equipped.owner == *victim {
                        // Drop their stuff
                        if let Some(pos) = pos {
                            to_drop.push((entity, pos.clone()));
                        }
                    }
                }
                for (entity, backpack) in (&entities, &carried).join() {
                    if backpack.owner == *victim {
                        // Drop their stuff
                        if let Some(pos) = pos {
                            to_drop.push((entity, pos.clone()));
                        }
                    }
                }

                if let Some(table) = loot_tables.get(*victim) {
                    let drop_finder = crate::raws::get_item_drop(
                        &crate::raws::RAWS.lock().unwrap(),
                        &mut rng,
                        &table.table
                    );
                    if let Some(tag) = drop_finder {
                        if let Some(pos) = pos {
                            to_spawn.push((tag, pos.clone()));
                        }
                    }
                }
            }

            for drop in to_drop.iter() {
                equipped.remove(drop.0);
                carried.remove(drop.0);
                positions.insert(drop.0, drop.1.clone()).expect("Unable to insert position");
            }
        }
        {
            for spawn in to_spawn.iter() {
                crate::raws::spawn_named_item(
                    &crate::raws::RAWS.lock().unwrap(),
                    ecs,
                    &spawn.0,
                    crate::raws::SpawnType::AtPosition{ x: spawn.1.x, y: spawn.1.y }
                );
            }
        }

        for victim in dead {
            ecs.delete_entity(victim).expect("Unable to delete");
        }
    }
}
