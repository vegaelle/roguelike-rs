use specs::prelude::*;
use super::{RunState, Entities, HungerClock, HungerState, gamelog::GameLog, SufferDamage};

pub const HUNGER_MAX_DURATION: i32 = 200;

pub struct Hunger {}

impl<'a> System<'a> for Hunger {
    type SystemData = (Entities<'a>,
                       WriteStorage<'a, HungerClock>,
                       WriteExpect<'a, GameLog>,
                       ReadExpect<'a, RunState>,
                       WriteStorage<'a, SufferDamage>,
                       ReadExpect<'a, Entity>,
                      );

    fn run(&mut self, data: Self::SystemData) {
        let (entities, mut hunger, mut gamelog, runstate, mut inflict_damage, player_entity) = data;

        for (entity, mut hunger) in (&entities, &mut hunger).join() {
            let mut proceed = false;

            match *runstate {
                RunState::PlayerTurn => {
                    if entity == *player_entity {
                        proceed = true;
                    }
                }
                RunState::MonsterTurn => {
                    if entity != *player_entity {
                        proceed = true;
                    }
                }
                _ => {}
            }
            if proceed {
                hunger.duration -= 1;
                if hunger.duration <= 0 {
                    match hunger.state {
                        HungerState::WellFed => {
                            hunger.state = HungerState::Normal;
                            hunger.duration = HUNGER_MAX_DURATION;
                            if entity == *player_entity {
                                gamelog.entries.push(String::from("You are no longer well fed."));
                            }
                        }
                        HungerState::Normal => {
                            hunger.state = HungerState::Hungry;
                            hunger.duration = HUNGER_MAX_DURATION;
                            if entity == *player_entity {
                                gamelog.entries.push(String::from("You are hungry."));
                            }
                        }
                        HungerState::Hungry => {
                            hunger.state = HungerState::Starving;
                            hunger.duration = HUNGER_MAX_DURATION;
                            if entity == *player_entity {
                                gamelog.entries.push(String::from("You are starving!"));
                            }
                        }
                        HungerState::Starving => {
                            if entity == *player_entity {
                                gamelog.entries.push(String::from("Your hunger pangs are getting painful! You suffer 1 hp damage."));
                            }
                            SufferDamage::new_damage(&mut inflict_damage, entity, 1);
                        }
                    }
                }
            }
        }
    }
}

