use rltk::{Rltk, GameState, Point};
use specs::prelude::*;
use specs::saveload::{SimpleMarker, SimpleMarkerAllocator};
use components::*;
use map::*;
use player::*;
use visibility_system::VisibilitySystem;
use monster_ai_system::MonsterAI;
use map_indexing_system::MapIndexingSystem;
use melee_combat_system::MeleeCombatSystem;
use damage_system::DamageSystem;
use inventory_system::{ItemCollectionSystem, ItemUseSystem, ItemDropSystem, ItemRemoveSystem};
use hunger_system::Hunger;
use entry_trigger_system::EntryTriggerSystem;
use bystander_ai_system::BystanderAI;
use animal_ai_system::AnimalAI;
use settings::{Settings, save_settings, load_settings};
use gamesystem::*;

#[macro_use]
extern crate lazy_static;

mod components;
mod map;
mod player;
mod visibility_system;
mod monster_ai_system;
mod map_indexing_system;
mod melee_combat_system;
mod damage_system;
mod gui;
mod gamelog;
mod spawner;
mod inventory_system;
mod saveload_system;
mod random_table;
mod particle_system;
mod hunger_system;
mod rex_assets;
mod entry_trigger_system;
mod bystander_ai_system;
mod animal_ai_system;
mod camera;
mod settings;
mod gamesystem;

pub mod map_builders;
pub mod raws;

#[cfg(debug_assertions)]
const DEBUG: bool = true;

#[cfg(not(debug_assertions))]
const DEBUG: bool = false;

#[cfg(debug_assertions)]
const SHOW_MAPGEN_VISUALIZER: bool = true;

#[cfg(not(debug_assertions))]
const SHOW_MAPGEN_VISUALIZER: bool = false;

#[derive(PartialEq, Copy, Clone)]
pub enum RunState {
    AwaitingInput,
    PreRun,
    PlayerTurn,
    MonsterTurn,
    ShowInventory,
    ShowDropItem,
    ShowTargeting { range: i32, item: Entity },
    MainMenu { menu_selection: gui::MainMenuSelection },
    SaveGame,
    NextLevel,
    ShowRemoveItem,
    GameOver,
    MagicMapReveal { row: i32 },
    MapGeneration,
    SettingsMenu { menu_selection: gui::SettingsMenuSelection, edit_mode: bool },
}

pub struct State {
    ecs: World,
    mapgen_next_state: Option<RunState>,
    mapgen_history: Vec<Map>,
    mapgen_index: usize,
    mapgen_timer: f32,
    settings: Settings,
}


impl State {

    pub fn new() -> State {
        let settings = load_settings();
        State {
            ecs: World::new(),
            mapgen_next_state: None,
            mapgen_history: Vec::new(),
            mapgen_index: 0,
            mapgen_timer: 0.0,
            settings
        }
    }

    fn run_systems(&mut self) {
        let mut vis = VisibilitySystem {};
        vis.run_now(&self.ecs);
        let mut mob = MonsterAI {};
        mob.run_now(&self.ecs);
        let mut bystander = BystanderAI {};
        bystander.run_now(&self.ecs);
        let mut animal = AnimalAI {};
        animal.run_now(&self.ecs);
        let mut mapindex = MapIndexingSystem {};
        mapindex.run_now(&self.ecs);
        let mut melee = MeleeCombatSystem {};
        melee.run_now(&self.ecs);
        let mut damage = DamageSystem {};
        damage.run_now(&self.ecs);
        let mut pickup = ItemCollectionSystem {};
        pickup.run_now(&self.ecs);
        let mut use_items = ItemUseSystem {};
        use_items.run_now(&self.ecs);
        let mut drop_items = ItemDropSystem {};
        drop_items.run_now(&self.ecs);
        let mut remove_items = ItemRemoveSystem {};
        remove_items.run_now(&self.ecs);
        let mut particles = particle_system::ParticleSpawnSystem {};
        particles.run_now(&self.ecs);
        let mut hunger = Hunger {};
        hunger.run_now(&self.ecs);
        let mut trigger = EntryTriggerSystem {};
        trigger.run_now(&self.ecs);
        self.ecs.maintain();
    }

    fn entities_to_remove_on_next_level(&mut self) -> Vec<Entity> {
        let entities = self.ecs.entities();
        let player = self.ecs.read_storage::<Player>();
        let backpack = self.ecs.read_storage::<InBackpack>();
        let player_entity = self.ecs.fetch::<Entity>();
        let equipped = self.ecs.read_storage::<Equipped>();

        let mut to_delete: Vec<Entity> = Vec::new();
        for entity in entities.join() {
            let mut should_delete = true;

            // Don’t delete the player
            let p = player.get(entity);
            if let Some(_p) = p {
                should_delete = false;
            }

            // Don’t delete the player’s equipment
            let bp = backpack.get(entity);
            if let Some(bp) = bp {
                if bp.owner == *player_entity {
                    should_delete = false;
                }
            }

            let eq = equipped.get(entity);
            if let Some(eq) = eq {
                if eq.owner == *player_entity {
                    should_delete = false;
                }
            }

            if should_delete {
                to_delete.push(entity);
            }
        }

        to_delete
    }

    fn goto_next_level(&mut self) {
        // Delete entities that aren’t the player or their equipment
        let to_delete = self.entities_to_remove_on_next_level();
        for target in to_delete {
            self.ecs.delete_entity(target).expect("Unable to delete entity");
        }

        let current_depth;
        {
            let worldmap_resource = self.ecs.fetch::<Map>();
            current_depth = worldmap_resource.depth;
        }
        self.generate_world_map(current_depth + 1);

        // Notify the player
        let mut gamelog = self.ecs.fetch_mut::<gamelog::GameLog>();
        gamelog.entries.push(String::from("You descend to the next level."));
    }

    fn game_over_cleanup(&mut self) {
        // Delete everything
        let mut to_delete = Vec::new();
        for e in self.ecs.entities().join() {
            to_delete.push(e);
        }
        for del in to_delete.iter() {
            self.ecs.delete_entity(*del).expect("Deletion failed");
        }

        // Spawn a new player
        {
            let player_entity = spawner::player(&mut self.ecs, 0, 0);
            let mut player_entity_writer = self.ecs.write_resource::<Entity>();
            *player_entity_writer = player_entity;
        }

        self.generate_world_map(1);
    }

    fn generate_world_map(&mut self, new_depth: i32) {
        self.mapgen_index = 0;
        self.mapgen_timer = 0.0;
        self.mapgen_history.clear();
        let mut rng = self.ecs.write_resource::<rltk::RandomNumberGenerator>();
        // Build a new map and place the player
        let mut builder = map_builders::level_builder(new_depth, &mut rng, 80, 50);
        builder.build_map(&mut rng);
        std::mem::drop(rng);
        self.mapgen_history = builder.build_data.history.clone();
        let player_pos;
        {
            let mut worldmap_resource = self.ecs.write_resource::<Map>();
            *worldmap_resource = builder.build_data.map.clone();
            player_pos = builder.build_data.starting_position.as_mut().unwrap().clone();
        }

        // Spawn all the things
        builder.spawn_entities(&mut self.ecs);

        // Place the player and update resources
        let mut player_position = self.ecs.write_resource::<Point>();
        *player_position = Point::new(player_pos.x, player_pos.y);
        let mut position_components = self.ecs.write_storage::<Position>();
        let player_entity = self.ecs.fetch::<Entity>();
        let player_pos_comp = position_components.get_mut(*player_entity);
        if let Some(player_pos_comp) = player_pos_comp {
            player_pos_comp.x = player_pos.x;
            player_pos_comp.y = player_pos.y;
        }

        // Mark the player’s visibility as dirty
        let mut viewshed_components = self.ecs.write_storage::<Viewshed>();
        let vs = viewshed_components.get_mut(*player_entity);
        if let Some(vs) = vs {
            vs.dirty = true;
        }
    }

    fn clear_key(&mut self, key: rltk::VirtualKeyCode) {
        if Some(key) == self.settings.move_up { self.settings.move_up = None; }
        if Some(key) == self.settings.move_right { self.settings.move_right = None; }
        if Some(key) == self.settings.move_down { self.settings.move_down = None; }
        if Some(key) == self.settings.move_left { self.settings.move_left = None; }
        if Some(key) == self.settings.move_up_right { self.settings.move_up_right = None; }
        if Some(key) == self.settings.move_down_right { self.settings.move_down_right = None; }
        if Some(key) == self.settings.move_down_left { self.settings.move_down_left = None; }
        if Some(key) == self.settings.move_up_left { self.settings.move_up_left = None; }
        if Some(key) == self.settings.take_stairs { self.settings.take_stairs = None; }
        if Some(key) == self.settings.pickup_item { self.settings.pickup_item = None; }
        if Some(key) == self.settings.inventory { self.settings.inventory = None; }
        if Some(key) == self.settings.drop_item { self.settings.drop_item = None; }
        if Some(key) == self.settings.unequip_item { self.settings.unequip_item = None; }
        if Some(key) == self.settings.main_menu { self.settings.main_menu = None; }
        if Some(key) == self.settings.wait { self.settings.wait = None; }
    }

    fn set_key_name(&mut self, setting: gui::SettingsMenuSelection, key: rltk::VirtualKeyCode) {
        if setting == gui::SettingsMenuSelection::MoveUp { self.settings.move_up = Some(key); }
        if setting == gui::SettingsMenuSelection::MoveRight { self.settings.move_right = Some(key); }
        if setting == gui::SettingsMenuSelection::MoveDown { self.settings.move_down = Some(key); }
        if setting == gui::SettingsMenuSelection::MoveLeft { self.settings.move_left = Some(key); }
        if setting == gui::SettingsMenuSelection::MoveUpRight { self.settings.move_up_right = Some(key); }
        if setting == gui::SettingsMenuSelection::MoveDownRight { self.settings.move_down_right = Some(key); }
        if setting == gui::SettingsMenuSelection::MoveDownLeft { self.settings.move_down_left = Some(key); }
        if setting == gui::SettingsMenuSelection::MoveUpLeft { self.settings.move_up_left = Some(key); }
        if setting == gui::SettingsMenuSelection::TakeStairs { self.settings.take_stairs = Some(key); }
        if setting == gui::SettingsMenuSelection::PickupItem { self.settings.pickup_item = Some(key); }
        if setting == gui::SettingsMenuSelection::Inventory { self.settings.inventory = Some(key); }
        if setting == gui::SettingsMenuSelection::DropItem { self.settings.drop_item = Some(key); }
        if setting == gui::SettingsMenuSelection::UnequipItem { self.settings.unequip_item = Some(key); }
        if setting == gui::SettingsMenuSelection::MainMenu { self.settings.main_menu = Some(key); }
        if setting == gui::SettingsMenuSelection::Wait { self.settings.wait = Some(key); }
    }

}

impl GameState for State {
    fn tick(&mut self, ctx : &mut Rltk) {
        let mut newrunstate;
        {
            let runstate = self.ecs.fetch::<RunState>();
            newrunstate = *runstate;
        }
        ctx.cls();

        particle_system::cull_dead_particles(&mut self.ecs, ctx);

        match newrunstate {
            RunState::MainMenu{..} => {}
            RunState::GameOver{..} => {}
            RunState::SettingsMenu{..} => {}
            _ => {
                // draw_map(&self.ecs.fetch::<Map>(), ctx);
                // {
                //     let positions = self.ecs.read_storage::<Position>();
                //     let renderables = self.ecs.read_storage::<Renderable>();
                //     let hidden = self.ecs.read_storage::<Hidden>();
                //     let map = self.ecs.fetch::<Map>();

                //     let mut data = (&positions, &renderables, !&hidden).join().collect::<Vec<_>>();
                //     data.sort_by(|&a, &b| b.1.render_order.cmp(&a.1.render_order));
                //     for (pos, render, _hidden) in data.iter() {
                //         let idx = map.xy_idx(pos.x, pos.y);
                //         if map.visible_tiles[idx] { ctx.set(pos.x, pos.y, render.fg, render.bg, render.glyph) }
                //     }
                camera::render_camera(&self.ecs, ctx);
                gui::draw_ui(&self.ecs, ctx);
                // }
            }
        }


        match newrunstate {
            RunState::PreRun => {
                self.run_systems();
                self.ecs.maintain();
                newrunstate = RunState::AwaitingInput;
            }
            RunState::AwaitingInput => {
                newrunstate = player_input(self, ctx);
            }
            RunState::PlayerTurn => {
                self.run_systems();
                self.ecs.maintain();
                match *self.ecs.fetch::<RunState>() {
                    RunState::MagicMapReveal { .. } => newrunstate = RunState::MagicMapReveal { row: 0 },
                    _ => newrunstate = RunState::MonsterTurn
                }
            }
            RunState::MonsterTurn => {
                self.run_systems();
                self.ecs.maintain();
                newrunstate = RunState::AwaitingInput;
            }
            RunState::ShowInventory => {
                let result = gui::show_inventory(self, ctx);
                match result.0 {
                    gui::ItemMenuResult::Cancel => newrunstate = RunState::AwaitingInput,
                    gui::ItemMenuResult::NoResponse => {}
                    gui::ItemMenuResult::Selected => {
                        let item_entity = result.1.unwrap();
                        let is_ranged = self.ecs.read_storage::<Ranged>();
                        let is_item_ranged = is_ranged.get(item_entity);
                        if let Some(is_item_ranged) = is_item_ranged {
                            newrunstate = RunState::ShowTargeting { range: is_item_ranged.range, item: item_entity };
                        } else {
                            let mut intent = self.ecs.write_storage::<WantsToUseItem>();
                            intent.insert(*self.ecs.fetch::<Entity>(), WantsToUseItem { item: item_entity, target: None }).expect("Unable to insert intent");
                            newrunstate = RunState::PlayerTurn;
                        }
                    }
                }
            }
            RunState::ShowDropItem => {
                let result = gui::drop_items_menu(self, ctx);
                match result.0 {
                    gui::ItemMenuResult::Cancel => newrunstate = RunState::AwaitingInput,
                    gui::ItemMenuResult::NoResponse => {}
                    gui::ItemMenuResult::Selected => {
                        let item_entity = result.1.unwrap();
                        let mut intent = self.ecs.write_storage::<WantsToDropItem>();
                        intent.insert(*self.ecs.fetch::<Entity>(), WantsToDropItem { item: item_entity }).expect("Unable to insert intent");
                        newrunstate = RunState::PlayerTurn;
                    }
                }
            }
            RunState::ShowTargeting{range, item} => {
                let result = gui::ranged_target(self, ctx, range);
                match result.0 {
                    gui::ItemMenuResult::Cancel => newrunstate = RunState::AwaitingInput,
                    gui::ItemMenuResult::NoResponse => {},
                    gui::ItemMenuResult::Selected => {
                        let mut intent = self.ecs.write_storage::<WantsToUseItem>();
                        intent.insert(*self.ecs.fetch::<Entity>(), WantsToUseItem { item, target: result.1 }).expect("Unable to insert intent");
                        newrunstate = RunState::PlayerTurn;
                    }
                }
            }
            RunState::MainMenu{ .. } => {
                let result = gui::main_menu(self, ctx);
                match result {
                    gui::MainMenuResult::NoSelection { selected } => newrunstate = RunState::MainMenu { menu_selection: selected },
                    gui::MainMenuResult::Selected { selected } => {
                        match selected {
                            gui::MainMenuSelection::NewGame => {
                                self.mapgen_next_state = Some(RunState::PreRun);
                                self.mapgen_index = 0;
                                self.mapgen_history = Vec::new();
                                self.mapgen_timer = 0.0;
                                newrunstate = RunState::MapGeneration;
                                self.generate_world_map(1);
                            }
                            gui::MainMenuSelection::LoadGame => {
                                saveload_system::load_game(&mut self.ecs);
                                newrunstate = RunState::AwaitingInput;
                                saveload_system::delete_save();
                            }
                            gui::MainMenuSelection::Settings => {
                                newrunstate = RunState::SettingsMenu { menu_selection: gui::SettingsMenuSelection::MoveUp, edit_mode: false };
                            }
                            gui::MainMenuSelection::Quit => { ::std::process::exit(0); }
                        }
                    }
                }
            }
            RunState::SaveGame => {
                saveload_system::save_game(&mut self.ecs);
                newrunstate = RunState::MainMenu { menu_selection: gui::MainMenuSelection::LoadGame };
            }
            RunState::NextLevel => {
                self.goto_next_level();
                // newrunstate = RunState::PreRun;
                newrunstate = RunState::MapGeneration {};
                self.mapgen_next_state = Some(RunState::PreRun {});
            }
            RunState::ShowRemoveItem => {
                let result = gui::remove_item_menu(self, ctx);
                match result.0 {
                    gui::ItemMenuResult::Cancel => newrunstate = RunState::AwaitingInput,
                    gui::ItemMenuResult::NoResponse => {},
                    gui::ItemMenuResult::Selected => {
                        let item_entity = result.1.unwrap();
                        let mut intent = self.ecs.write_storage::<WantsToRemoveItem>();
                        intent.insert(*self.ecs.fetch::<Entity>(), WantsToRemoveItem { item: item_entity }).expect("Unable to insert intent");
                        newrunstate = RunState::PlayerTurn;
                    }
                }
            }
            RunState::GameOver => {
                let result = gui::game_over(ctx);
                match result {
                    gui::GameOverResult::NoSelection => {}
                    gui::GameOverResult::QuitToMenu => {
                        self.game_over_cleanup();
                        newrunstate = RunState::MainMenu { menu_selection: gui::MainMenuSelection::NewGame };
                        // newrunstate = RunState::MainMenu {};
                        // self.mapgen_next_state = Some(RunState::MainMenu { menu_selection: gui::MainMenuSelection::NewGame });
                    }
                }
            }
            RunState::MagicMapReveal{ row } => {
                let mut map = self.ecs.fetch_mut::<Map>();
                for x in 0..map.width {
                    let idx = map.xy_idx(x as i32, row);
                    map.revealed_tiles[idx] = true;
                }
                if row == map.height - 1 {
                    newrunstate = RunState::MonsterTurn;
                } else {
                    newrunstate = RunState::MagicMapReveal { row: row + 1 };
                }
            }
            RunState::MapGeneration => {
                if !SHOW_MAPGEN_VISUALIZER {
                    newrunstate = self.mapgen_next_state.unwrap();
                }
                ctx.cls();
                // draw_map(&self.mapgen_history[self.mapgen_index], ctx);
                if self.mapgen_index < self.mapgen_history.len() { camera::render_debug_map(&self.mapgen_history[self.mapgen_index], ctx); }

                self.mapgen_timer += ctx.frame_time_ms;
                if self.mapgen_timer > 300.0 {
                    self.mapgen_timer = 0.0;
                    self.mapgen_index += 1;
                    if self.mapgen_index >= self.mapgen_history.len() {
                        newrunstate = self.mapgen_next_state.unwrap();
                    }
                }
            }
            RunState::SettingsMenu{ .. } => {
                let result = gui::settings_menu(self, ctx);
                match result {
                    gui::SettingsMenuResult::Cancel => newrunstate = RunState::MainMenu { menu_selection: gui::MainMenuSelection::Settings },
                    gui::SettingsMenuResult::Saved { selected, key } => {
                        self.clear_key(key);
                        self.set_key_name(selected, key);
                        save_settings(&self.settings);
                        newrunstate = RunState::SettingsMenu { menu_selection: selected, edit_mode: false };
                    }
                    gui::SettingsMenuResult::Selected { selected, edit_mode } => newrunstate = RunState::SettingsMenu { menu_selection: selected, edit_mode },
                }
            }
        }

        {
            let mut runwriter = self.ecs.write_resource::<RunState>();
            *runwriter = newrunstate;
        }
        DamageSystem::delete_the_dead(&mut self.ecs);

    }
}

fn main() -> rltk::BError {
    use rltk::RltkBuilder;
    let mut context = RltkBuilder::simple(80, 60)
        .unwrap()
        .with_title("Roguelike Tutorial")
        .with_fullscreen(true)
        .build()?;
    context.with_post_scanlines(true);
    let mut gs = State::new();
    gs.ecs.register::<Position>();
    gs.ecs.register::<Renderable>();
    gs.ecs.register::<Player>();
    gs.ecs.register::<Viewshed>();
    gs.ecs.register::<Monster>();
    gs.ecs.register::<Bystander>();
    gs.ecs.register::<Vendor>();
    gs.ecs.register::<Name>();
    gs.ecs.register::<BlocksTile>();
    gs.ecs.register::<WantsToMelee>();
    gs.ecs.register::<SufferDamage>();
    gs.ecs.register::<Item>();
    gs.ecs.register::<Consumable>();
    gs.ecs.register::<ProvidesHealing>();
    gs.ecs.register::<WantsToPickupItem>();
    gs.ecs.register::<InBackpack>();
    gs.ecs.register::<WantsToUseItem>();
    gs.ecs.register::<WantsToDropItem>();
    gs.ecs.register::<Consumable>();
    gs.ecs.register::<Ranged>();
    gs.ecs.register::<InflictsDamage>();
    gs.ecs.register::<AreaOfEffect>();
    gs.ecs.register::<Confusion>();
    gs.ecs.register::<SimpleMarker<SerializeMe>>();
    gs.ecs.register::<SerializationHelper>();
    gs.ecs.register::<Equippable>();
    gs.ecs.register::<Equipped>();
    gs.ecs.register::<MeleeWeapon>();
    gs.ecs.register::<Wearable>();
    gs.ecs.register::<WantsToRemoveItem>();
    gs.ecs.register::<ParticleLifetime>();
    gs.ecs.register::<HungerClock>();
    gs.ecs.register::<ProvidesFood>();
    gs.ecs.register::<MagicMapper>();
    gs.ecs.register::<Hidden>();
    gs.ecs.register::<EntryTrigger>();
    gs.ecs.register::<EntityMoved>();
    gs.ecs.register::<SingleActivation>();
    gs.ecs.register::<BlocksVisibility>();
    gs.ecs.register::<Door>();
    gs.ecs.register::<Quips>();
    gs.ecs.register::<Attributes>();
    gs.ecs.register::<Skills>();
    gs.ecs.register::<Pools>();
    gs.ecs.register::<LootTable>();
    gs.ecs.register::<NaturalAttackDefense>();
    gs.ecs.register::<Carnivore>();
    gs.ecs.register::<Herbivore>();
    gs.ecs.insert(SimpleMarkerAllocator::<SerializeMe>::new());
    gs.ecs.insert(rex_assets::RexAssets::new());
    gs.ecs.insert(rltk::RandomNumberGenerator::new());

    raws::load_raws();

    gs.ecs.insert(Map::new(1, 64, 64, "New Map"));
    gs.ecs.insert(Point::new(0, 0));

    let player_entity = spawner::player(&mut gs.ecs, 0, 0);
    gs.ecs.insert(player_entity);

    // gs.ecs.insert(RunState::MainMenu { menu_selection: gui::MainMenuSelection::NewGame });
    gs.ecs.insert(RunState::MainMenu { menu_selection: gui::MainMenuSelection::NewGame });
    gs.ecs.insert(gamelog::GameLog { entries : vec!["Welcome to Rusty Roguelike".to_string()] });
    gs.ecs.insert(particle_system::ParticleBuilder::new());

    // gs.generate_world_map(1);

    rltk::main_loop(context, gs)
}
