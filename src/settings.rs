use std::fs;
use serde::{Deserializer, Serializer};
use serde_with::{serde_as, SerializeAs, DeserializeAs};


#[serde(remote="rltk::VirtualKeyCode")]
#[derive(serde::Serialize, serde::Deserialize)]
enum VirtualKeyCodeDef {
/// The '1' key over the letters.
    Key1,
    /// The '2' key over the letters.
    Key2,
    /// The '3' key over the letters.
    Key3,
    /// The '4' key over the letters.
    Key4,
    /// The '5' key over the letters.
    Key5,
    /// The '6' key over the letters.
    Key6,
    /// The '7' key over the letters.
    Key7,
    /// The '8' key over the letters.
    Key8,
    /// The '9' key over the letters.
    Key9,
    /// The '0' key over the 'O' and 'P' keys.
    Key0,

    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,

    /// The Escape key, next to F1.
    Escape,

    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    F13,
    F14,
    F15,
    F16,
    F17,
    F18,
    F19,
    F20,
    F21,
    F22,
    F23,
    F24,

    /// Print Screen/SysRq.
    Snapshot,
    /// Scroll Lock.
    Scroll,
    /// Pause/Break key, next to Scroll lock.
    Pause,

    /// `Insert`, next to Backspace.
    Insert,
    Home,
    Delete,
    End,
    PageDown,
    PageUp,

    Left,
    Up,
    Right,
    Down,

    /// The Backspace key, right over Enter.
    // TODO: rename
    Back,
    /// The Enter key.
    Return,
    /// The space bar.
    Space,

    /// The "Compose" key on Linux.
    Compose,

    Caret,

    Numlock,
    Numpad0,
    Numpad1,
    Numpad2,
    Numpad3,
    Numpad4,
    Numpad5,
    Numpad6,
    Numpad7,
    Numpad8,
    Numpad9,
    NumpadAdd,
    NumpadDivide,
    NumpadDecimal,
    NumpadComma,
    NumpadEnter,
    NumpadEquals,
    NumpadMultiply,
    NumpadSubtract,

    AbntC1,
    AbntC2,
    Apostrophe,
    Apps,
    Asterisk,
    At,
    Ax,
    Backslash,
    Calculator,
    Capital,
    Colon,
    Comma,
    Convert,
    Equals,
    Grave,
    Kana,
    Kanji,
    LAlt,
    LBracket,
    LControl,
    LShift,
    LWin,
    Mail,
    MediaSelect,
    MediaStop,
    Minus,
    Mute,
    MyComputer,
    // also called "Next"
    NavigateForward,
    // also called "Prior"
    NavigateBackward,
    NextTrack,
    NoConvert,
    OEM102,
    Period,
    PlayPause,
    Plus,
    Power,
    PrevTrack,
    RAlt,
    RBracket,
    RControl,
    RShift,
    RWin,
    Semicolon,
    Slash,
    Sleep,
    Stop,
    Sysrq,
    Tab,
    Underline,
    Unlabeled,
    VolumeDown,
    VolumeUp,
    Wake,
    WebBack,
    WebFavorites,
    WebForward,
    WebHome,
    WebRefresh,
    WebSearch,
    WebStop,
    Yen,
    Copy,
    Paste,
    Cut,
}

impl SerializeAs<rltk::VirtualKeyCode> for VirtualKeyCodeDef {
    fn serialize_as<S>(value: &rltk::VirtualKeyCode, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer
    {
        VirtualKeyCodeDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, rltk::VirtualKeyCode> for VirtualKeyCodeDef
{
    fn deserialize_as<D>(deserializer: D) -> Result<rltk::VirtualKeyCode, D::Error>
        where
            D: Deserializer<'de>
    {
        VirtualKeyCodeDef::deserialize(deserializer)
    }
}

#[serde_as]
#[derive(serde::Serialize, serde::Deserialize)]
pub struct Settings {
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub move_up: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub move_right: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub move_down: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub move_left: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub move_up_right: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub move_down_right: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub move_down_left: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub move_up_left: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub take_stairs: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub pickup_item: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub inventory: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub drop_item: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub unequip_item: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub main_menu: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub wait: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action1: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action2: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action3: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action4: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action5: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action6: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action7: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action8: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action9: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action10: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action11: Option<rltk::VirtualKeyCode>,
    #[serde_as(as="Option<VirtualKeyCodeDef>")]
    pub action12: Option<rltk::VirtualKeyCode>,
}

pub fn load_settings() -> Settings {

    let data = fs::read_to_string("settings.json");
    if let Ok(data) = data {
        let settings = serde_json::from_str(&data);
        if let Ok(settings) = settings {
            return settings;
        }
    }

    let settings: Settings;
    {
        use rltk::VirtualKeyCode::*;
        settings = Settings {
            move_up: Some(L),
            move_right: Some(Semicolon),
            move_down: Some(K),
            move_left: Some(J),
            move_up_right: Some(O),
            move_down_right: Some(Period),
            move_down_left: Some(Comma),
            move_up_left: Some(I),
            take_stairs: Some(V),
            pickup_item: Some(U),
            inventory: Some(M),
            drop_item: Some(P),
            unequip_item: Some(R),
            main_menu: Some(Escape),
            wait: Some(Space),
            action1: Some(F1),
            action2: Some(F2),
            action3: Some(F3),
            action4: Some(F4),
            action5: Some(F5),
            action6: Some(F6),
            action7: Some(F7),
            action8: Some(F8),
            action9: Some(F9),
            action10: Some(F10),
            action11: Some(F11),
            action12: Some(F12),
        };

    }

    settings
}

pub fn save_settings(settings: &Settings) {
    let serialized = serde_json::to_string(settings).unwrap();
    fs::write("settings.json", serialized).unwrap();
}
