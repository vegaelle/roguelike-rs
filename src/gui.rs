use rltk::{RGB, Rltk, Point, VirtualKeyCode};
use specs::prelude::*;
use super::{Pools, Player, Map, Name, Position, InBackpack, State, Viewshed,
    RunState, Equipped, HungerClock, HungerState, Hidden, SHOW_MAPGEN_VISUALIZER,
    camera::get_screen_bounds, Attributes, Attribute, Consumable};
use super::gamelog::GameLog;
use super::rex_assets::RexAssets;
use std::collections::HashMap;



pub fn draw_ui(ecs: &World, ctx: &mut Rltk) {
    use rltk::to_cp437;
    let box_gray: RGB = RGB::from_hex("#999999").expect("Oops");
    let black = RGB::named(rltk::BLACK);
    let white = RGB::named(rltk::WHITE);

    draw_hollow_box(ctx, 0, 0, 79, 59, box_gray, black); // Overall box
    draw_hollow_box(ctx, 0, 0, 49, 45, box_gray, black); // Map box
    draw_hollow_box(ctx, 0, 45, 79, 14, box_gray, black); // Log box
    draw_hollow_box(ctx, 49, 0, 30, 8, box_gray, black); // Top-right panel

    ctx.set(0, 45, box_gray, black, to_cp437('├'));
    ctx.set(49, 8, box_gray, black, to_cp437('├'));
    ctx.set(49, 0, box_gray, black, to_cp437('┬'));
    ctx.set(49, 45, box_gray, black, to_cp437('┴'));
    ctx.set(79, 8, box_gray, black, to_cp437('┤'));
    ctx.set(79, 45, box_gray, black, to_cp437('┤'));

    // Draw the map name
    let map = ecs.fetch::<Map>();
    let name_length = map.name.len() + 2;
    let x_pos = (22 - (name_length / 2)) as i32;
    ctx.set(x_pos, 0, box_gray, black, to_cp437('┤'));
    ctx.set(x_pos + name_length as i32 - 1, 0, box_gray, black, to_cp437('├'));
    ctx.print_color(x_pos + 1, 0, white, black, &map.name);

    let player_entity = ecs.fetch::<Entity>();
    let pools_storage = ecs.read_storage::<Pools>();
    let player = ecs.read_storage::<Player>();
    for (_player, stats) in (&player, &pools_storage).join() {
        let health = format!("Health: {}/{}", stats.hit_points.current, stats.hit_points.max);
        let mana = format!("Mana: {}/{}", stats.mana.current, stats.mana.max);
        ctx.print_color(50, 1, white, black, &health);
        ctx.print_color(50, 2, white, black, &mana);
        ctx.draw_bar_horizontal(64, 1, 14, stats.hit_points.current, stats.hit_points.max, RGB::named(rltk::RED), RGB::named(rltk::BLACK));
        ctx.draw_bar_horizontal(64, 2, 14, stats.mana.current, stats.mana.max, RGB::named(rltk::BLUE), RGB::named(rltk::BLACK));
    }

    // Attributes
    let attributes = ecs.read_storage::<Attributes>();
    let attr = attributes.get(*player_entity).unwrap();
    draw_attribute("Might:", &attr.might, 4, ctx);
    draw_attribute("Quickness:", &attr.quickness, 5, ctx);
    draw_attribute("Fitness:", &attr.fitness, 6, ctx);
    draw_attribute("Intelligence:", &attr.intelligence, 7, ctx);

    // Equipped
    let mut y = 9;
    let equipped = ecs.read_storage::<Equipped>();
    let name = ecs.read_storage::<Name>();
    for (equipped_by, item_name) in (&equipped, &name).join() {
        if equipped_by.owner == *player_entity {
            ctx.print_color(50, y, white, black, &item_name.name);
            y += 1;
        }
    }

    // Consumables
    y += 1;
    let green = RGB::from_f32(0.0, 1.0, 0.0);
    let yellow = RGB::named(rltk::YELLOW);
    let consumables = ecs.read_storage::<Consumable>();
    let backpack = ecs.read_storage::<InBackpack>();
    let mut index = 1;
    let inventory = (&consumables, &backpack, &name).join().filter(|item| item.1.owner == *player_entity);
    let mut inventory_numbered: HashMap<String, i32> = HashMap::new();

    for (_consumable, _in_backpack, name) in inventory {
        let inventory_entry = inventory_numbered.entry(String::from(name.name.clone())).or_insert(0);
        *inventory_entry += 1;
    }
    let mut inventory_sorted = inventory_numbered.iter().collect::<Vec<(&String, &i32)>>();
    inventory_sorted.sort_by(|i, j| i.cmp(&j));
    for (item_name, count) in inventory_sorted.iter() {
        if index < 12 {
            ctx.print_color(50, y, yellow, black, &format!("F{}", index));
            if *count > &1 {
                ctx.print_color(53, y, green, black, &format!("{} ({})", item_name, count));
            } else {
                ctx.print_color(53, y, green, black, &format!("{}", item_name));
            }
            y += 1;
            index += 1;
        }
    }

    // Status
    let hunger = ecs.read_storage::<HungerClock>();
    let hc = hunger.get(*player_entity).unwrap();
    match hc.state {
        HungerState::WellFed => ctx.print_color(50, 44, RGB::named(rltk::GREEN), RGB::named(rltk::BLACK), String::from("Well fed")),
        HungerState::Normal => {}
        HungerState::Hungry => ctx.print_color(50, 44, RGB::named(rltk::ORANGE), RGB::named(rltk::BLACK), String::from("Hungry")),
        HungerState::Starving => ctx.print_color(50, 44, RGB::named(rltk::RED), RGB::named(rltk::BLACK), String::from("Starving")),
    }

    // Draw the log
    let log = ecs.fetch::<GameLog>();
    let mut y = 46;
    for s in log.entries.iter().rev() {
        if y < 59 { ctx.print(2, y, s); }
        y += 1;
    }

    draw_tooltips(ecs, ctx);

    // Draw mouse cursor
    let mouse_pos = ctx.mouse_pos();
    // ctx.set_bg(mouse_pos.0, mouse_pos.1, RGB::named(rltk::MAGENTA));
    // Debug: show path to pointed tile
    if SHOW_MAPGEN_VISUALIZER && ctx.shift {
        // let mut map = ecs.fetch_mut::<Map>();
        let (min_x, _max_x, min_y, _max_y) = get_screen_bounds(ecs, ctx);
        let mut mouse_map_pos = mouse_pos;
        mouse_map_pos.0 += min_x;
        mouse_map_pos.1 += min_y;
        if mouse_map_pos.0 > 1 && mouse_map_pos.0 < map.width
            && mouse_map_pos.1 > 1 && mouse_map_pos.1 < map.height {
            let player_pos = ecs.fetch::<Point>();
            let path = rltk::a_star_search(
                map.xy_idx(player_pos.x, player_pos.y) as i32,
                map.xy_idx(mouse_map_pos.0, mouse_map_pos.1) as i32,
                &*map
            );
            if path.success && path.steps.len() > 1 {
                for tile in path.steps.iter().skip(1) {
                    let x = ((*tile as i32) % map.width) - min_x;
                    let y = ((*tile as i32) / map.width) - min_y;
                    ctx.set(x, y, RGB::named(rltk::MAGENTA), RGB::named(rltk::BLACK), rltk::to_cp437('*'));
                }
            }
        }
    }

    return;
}

fn draw_tooltips(ecs: &World, ctx: &mut Rltk) {
    use rltk::to_cp437;

    let (min_x, _max_x, min_y, _max_y) = get_screen_bounds(ecs, ctx);
    let map = ecs.fetch::<Map>();
    let names = ecs.read_storage::<Name>();
    let positions = ecs.read_storage::<Position>();
    let hidden = ecs.read_storage::<Hidden>();
    let attributes = ecs.read_storage::<Attributes>();
    let pools = ecs.read_storage::<Pools>();
    let entities = ecs.entities();

    let mouse_pos = ctx.mouse_pos();
    let mut mouse_map_pos = mouse_pos;
    mouse_map_pos.0 += min_x;
    mouse_map_pos.1 += min_y;
    if mouse_map_pos.0 >= map.width || mouse_map_pos.1 >= map.height { return; }

    let mut tip_boxes: Vec<Tooltip> = Vec::new();
    for (entity, name, position, _hidden) in (&entities, &names, &positions, !&hidden).join() {
        let idx = map.xy_idx(position.x, position.y);
        if position.x == mouse_map_pos.0 && position.y == mouse_map_pos.1 && map.visible_tiles[idx] {
            let mut tip = Tooltip::new();
            tip.add(name.name.to_string());

            // Comment on attributes
            let attr = attributes.get(entity);
            if let Some(attr) = attr {
                let mut s = String::new();
                if attr.might.bonus < 0 { s += "Weak. " };
                if attr.might.bonus < 0 { s += "Strong. " };
                if attr.quickness.bonus < 0 { s += "Clumsy. " };
                if attr.quickness.bonus > 0 { s += "Agile. " };
                if attr.fitness.bonus < 0 { s += "Unhealthy. " };
                if attr.fitness.bonus > 0 { s += "Healthy. " };
                if attr.intelligence.bonus < 0 { s += "Unintelligent. " };
                if attr.intelligence.bonus > 0 { s += "Smart. " };
                if s.is_empty() { s += "Quite Average. " };
                tip.add(s);
            }
            // Comment on pools
            let stat = pools.get(entity);
            if let Some(stat) = stat {
                tip.add(format!("Level: {}", stat.level));
            }
            tip_boxes.push(tip);
        }
    }
    if tip_boxes.is_empty() { return; }

    let box_gray: RGB = RGB::from_hex("#999999").expect("Oops");
    let white = RGB::named(rltk::WHITE);

    let arrow;
    let arrow_x;
    let arrow_y = mouse_pos.1;
    if mouse_pos.0 < 40 {
        // Render to the left
        arrow = to_cp437('→');
        arrow_x = mouse_pos.0 - 1;
    } else {
        // Render to the right
        arrow = to_cp437('←');
        arrow_x = mouse_pos.0 + 1;
    }
    ctx.set(arrow_x, arrow_y, white, box_gray, arrow);

    let mut total_height = 0;
    for tt in tip_boxes.iter() {
        total_height += tt.height();
    }
    let mut y = mouse_pos.1 - (total_height / 2);
    while y + (total_height / 2) > 50 {
        y -= 1;
    }
    for tt in tip_boxes.iter() {
        let x = if mouse_pos.0 < 40 {
            mouse_pos.0 - (1 + tt.width())
        } else {
            mouse_pos.0 + (1 + tt.width())
        };
        tt.render(ctx, x, y);
        y += tt.height();
    }

    return;
}

#[derive(PartialEq, Copy, Clone)]
pub enum ItemMenuResult { Cancel, NoResponse, Selected }

pub fn show_inventory(gs: &mut State, ctx: &mut Rltk) -> (ItemMenuResult, Option<Entity>) {
    let player_entity = gs.ecs.fetch::<Entity>();
    let names = gs.ecs.read_storage::<Name>();
    let backpack = gs.ecs.read_storage::<InBackpack>();
    let entities = gs.ecs.entities();

    let inventory = (&entities, &backpack, &names).join().filter(|item| item.1.owner == *player_entity);
    let mut inventory_numbered: HashMap<String, Vec<Entity>> = HashMap::new();

    for (entity, _in_backpack, name) in inventory {
        let inventory_entry = inventory_numbered.entry(String::from(name.name.clone())).or_insert(Vec::new());
        inventory_entry.push(entity);
    }

    let count = inventory_numbered.len();

    let mut y = (25 - (count / 2)) as i32;
    ctx.draw_box(20, y - 2, 40, (count + 3) as i32, RGB::named(rltk::WHITE),  RGB::named(rltk::BLACK));
    ctx.print_color(23, y - 2, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), "Inventory");
    ctx.print_color(23, y + count as i32+1, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), "ESCAPE to cancel");

    let mut equippable: Vec<&String> = Vec::new();
    let mut j = 0;
    let mut equippable_names: Vec<&String> = inventory_numbered.keys().into_iter().collect();
    equippable_names.sort_by(|i, j| i.cmp(&j));
    // for (entity, _pack, name) in (&entities, &backpack, &names).join().filter(|item| item.1.owner == *player_entity) {
    for name in  equippable_names.iter() {
        ctx.set(22, y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), rltk::to_cp437('('));
        ctx.set(23, y, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), 97 + j as rltk::FontCharType);
        ctx.set(24, y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), rltk::to_cp437(')'));

        if inventory_numbered[*name].len() == 1 {
            ctx.print(26, y, format!("{}", name));
        } else {
            ctx.print(26, y, format!("{} ({})", name, inventory_numbered[*name].len()));
        }
        equippable.push(name);
        y += 1;
        j += 1;
    }

    match ctx.key {
        None => (ItemMenuResult::NoResponse, None),
        Some(key) => {
            match key {
                VirtualKeyCode::Escape => { (ItemMenuResult::Cancel, None) }
                _ => {
                    let selection = rltk::letter_to_option(key);
                    if selection > -1 && selection < count as i32 {
                        let item_stash = &inventory_numbered[equippable[selection as usize]];
                        if item_stash.len() > 0 {
                            return (ItemMenuResult::Selected, Some(item_stash[0]));
                        }
                        return (ItemMenuResult::Cancel, None);
                    }
                    (ItemMenuResult::NoResponse, None)
                }
            }
        }
    }
}

pub fn drop_items_menu(gs: &mut State, ctx: &mut Rltk) -> (ItemMenuResult, Option<Entity>) {
    let player_entity = gs.ecs.fetch::<Entity>();
    let names = gs.ecs.read_storage::<Name>();
    let backpack = gs.ecs.read_storage::<InBackpack>();
    let entities = gs.ecs.entities();

    let inventory = (&entities, &backpack, &names).join().filter(|item| item.1.owner == *player_entity);
    let mut inventory_numbered: HashMap<String, Vec<Entity>> = HashMap::new();
    for (entity, _in_backpack, name) in inventory {
        let inventory_entry = inventory_numbered.entry(String::from(name.name.clone())).or_insert(Vec::new());
        inventory_entry.push(entity);
    }

    let count = inventory_numbered.len();

    let mut y = (25 - (count / 2)) as i32;
    ctx.draw_box(20, y - 2, 40, (count + 3) as i32, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK));
    ctx.print_color(23, y - 2, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), "Drop Which Item?");
    ctx.print_color(23, y + count as i32 + 1, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), "ESCAPE to cancel");

    let mut equippable: Vec<&String> = Vec::new();
    let mut j = 0;
    // for (entity, _pack, name) in (&entities, &backpack, &names).join().filter(|item| item.1.owner == *player_entity) {
    let mut equippable_names: Vec<&String> = inventory_numbered.keys().into_iter().collect();
    equippable_names.sort_by(|i, j| i.cmp(&j));
    for name in  equippable_names.iter() {
        ctx.set(22, y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), rltk::to_cp437('('));
        ctx.set(23, y, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), 97 + j as rltk::FontCharType);
        ctx.set(24, y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), rltk::to_cp437(')'));

        if inventory_numbered[*name].len() == 1 {
            ctx.print(26, y, format!("{}", name));
        } else {
            ctx.print(26, y, format!("{} ({})", name, inventory_numbered[*name].len()));
        }
        equippable.push(name);
        y += 1;
        j += 1;
    }

    match ctx.key {
        None => (ItemMenuResult::NoResponse, None),
        Some(key) => {
            match key {
                VirtualKeyCode::Escape => (ItemMenuResult::Cancel, None),
                _ => {
                    let selection = rltk::letter_to_option(key);
                    if selection > -1 && selection < count as i32 {
                        let item_stash = &inventory_numbered[equippable[selection as usize]];
                        if item_stash.len() > 0 {
                            return (ItemMenuResult::Selected, Some(item_stash[0]));
                        }
                        return (ItemMenuResult::Cancel, None);
                    }
                    (ItemMenuResult::NoResponse, None)
                }
            }
        }
    }
}

pub fn ranged_target(gs: &mut State, ctx: &mut Rltk, range: i32) -> (ItemMenuResult, Option<Point>) {
    let (min_x, _max_x, min_y, _max_y) = get_screen_bounds(&gs.ecs, ctx);
    let player_entity = gs.ecs.fetch::<Entity>();
    let player_pos = gs.ecs.fetch::<Point>();
    let viewsheds = gs.ecs.read_storage::<Viewshed>();

    ctx.print_color(5, 0, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), "Select target:");

    let mut available_cells = Vec::new();
    let visible = viewsheds.get(*player_entity);
    if let Some(visible) = visible {
        // We have a viewshed
        for idx in visible.visible_tiles.iter() {
            let distance = rltk::DistanceAlg::Pythagoras.distance2d(*player_pos, *idx);
            if distance <= range as f32 {
                ctx.set_bg(idx.x - min_x, idx.y - min_y, RGB::named(rltk::BLUE));
                available_cells.push(idx);
            }
        }
    } else {
        return (ItemMenuResult::Cancel, None);
    }

    // Draw mouse cursor
    let mouse_pos = ctx.mouse_pos();
    let mut mouse_map_pos = mouse_pos;
    mouse_map_pos.0 += min_x;
    mouse_map_pos.1 += min_y;
    let mut valid_target = false;
    for idx in available_cells.iter() {
        if idx.x == mouse_map_pos.0 && idx.y == mouse_map_pos.1 { valid_target = true; }
    }
    if valid_target {
        ctx.set_bg(mouse_pos.0, mouse_pos.1, RGB::named(rltk::CYAN));
        if ctx.left_click {
            return (ItemMenuResult::Selected, Some(Point::new(mouse_map_pos.0, mouse_map_pos.1)));
        }
    } else {
        ctx.set_bg(mouse_pos.0, mouse_pos.1, RGB::named(rltk::RED));
        if ctx.left_click {
            return (ItemMenuResult::Cancel, None);
        }
    }

    (ItemMenuResult::NoResponse, None)
}

#[derive(PartialEq, Copy, Clone)]
pub enum MainMenuSelection { NewGame, LoadGame, Settings, Quit }

#[derive(PartialEq, Copy, Clone)]
pub enum MainMenuResult {
    NoSelection { selected: MainMenuSelection },
    Selected { selected: MainMenuSelection }
}

pub fn main_menu(gs: &mut State, ctx: &mut Rltk) -> MainMenuResult {
    let save_exists = super::saveload_system::does_save_exist();
    let runstate = gs.ecs.fetch::<RunState>();
    let assets = gs.ecs.fetch::<RexAssets>();

    ctx.render_xp_sprite(&assets.menu, 0, 5);

    let (x_chars, _y_chars) = ctx.get_char_size();

    let width = 30;

    ctx.draw_box_double((x_chars / 2) - (width / 2), 23, width, 10, RGB::named(rltk::WHEAT), RGB::named(rltk::BLACK));
    ctx.print_color_centered(25, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), "Rust Roguelike Tutorial");
    ctx.print_color_centered(26, RGB::named(rltk::CYAN), RGB::named(rltk::BLACK), "by Herbert Wolverson & Gordon");
    ctx.print_color_centered(27, RGB::named(rltk::GRAY), RGB::named(rltk::BLACK), "Use Up/Down Arrows and Enter");

    let mut y = 29;

    if let RunState::MainMenu { menu_selection: selection } = *runstate {
        if selection == MainMenuSelection::NewGame {
            ctx.print_color_centered(y, RGB::named(rltk::MAGENTA), RGB::named(rltk::BLACK), "Begin New Game");
        } else {
            ctx.print_color_centered(y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), "Begin New Game");
        }
        y += 1;

        if save_exists {
            if selection == MainMenuSelection::LoadGame {
                ctx.print_color_centered(y, RGB::named(rltk::MAGENTA), RGB::named(rltk::BLACK), "Load Game");
            } else {
                ctx.print_color_centered(y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), "Load Game");
            }
            y += 1;
        }

        if selection == MainMenuSelection::Settings {
            ctx.print_color_centered(y, RGB::named(rltk::MAGENTA), RGB::named(rltk::BLACK), "Settings");
        } else {
            ctx.print_color_centered(y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), "Settings");
        }
        y += 1;

        if selection == MainMenuSelection::Quit {
            ctx.print_color_centered(y, RGB::named(rltk::MAGENTA), RGB::named(rltk::BLACK), "Quit");
        } else {
            ctx.print_color_centered(y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), "Quit");
        }

        match ctx.key {
            None => return MainMenuResult::NoSelection { selected: selection },
            Some(key) => {
                match key {
                    VirtualKeyCode::Escape => { return MainMenuResult::NoSelection { selected: MainMenuSelection::Quit } }
                    VirtualKeyCode::Up => {
                        let mut newselection;
                        match selection {
                            MainMenuSelection::NewGame => newselection = MainMenuSelection::Quit,
                            MainMenuSelection::LoadGame => newselection = MainMenuSelection::NewGame,
                            MainMenuSelection::Settings => newselection = MainMenuSelection::LoadGame,
                            MainMenuSelection::Quit => newselection = MainMenuSelection::Settings
                        }
                        if newselection == MainMenuSelection::LoadGame && !save_exists {
                            newselection = MainMenuSelection::NewGame;
                        }
                        return MainMenuResult::NoSelection { selected: newselection };
                    }
                    VirtualKeyCode::Down => {
                        let mut newselection;
                        match selection {
                            MainMenuSelection::NewGame => newselection = MainMenuSelection::LoadGame,
                            MainMenuSelection::LoadGame => newselection = MainMenuSelection::Settings,
                            MainMenuSelection::Settings => newselection = MainMenuSelection::Quit,
                            MainMenuSelection::Quit => newselection = MainMenuSelection::NewGame
                        }
                        if newselection == MainMenuSelection::LoadGame && !save_exists {
                            newselection = MainMenuSelection::Settings;
                        }
                        return MainMenuResult::NoSelection { selected: newselection };
                    }
                    VirtualKeyCode::Return => return MainMenuResult::Selected { selected: selection },
                    _ => return MainMenuResult::NoSelection { selected: selection }
                }
            }
        }
    }

    MainMenuResult::NoSelection { selected: MainMenuSelection::NewGame }
}

#[derive(PartialEq, Copy, Clone, Eq, Hash, serde::Serialize, serde::Deserialize, Debug)]
pub enum SettingsMenuSelection { MoveUp, MoveRight, MoveDown, MoveLeft,
    MoveUpRight, MoveDownRight, MoveDownLeft, MoveUpLeft, TakeStairs,
    PickupItem, Inventory, DropItem, UnequipItem, MainMenu, Wait,
    Action1, Action2, Action3, Action4, Action5, Action6, Action7, Action8,
    Action9, Action10, Action11, Action12,
}

#[derive(PartialEq, Copy, Clone)]
pub enum SettingsMenuResult {
    Selected { selected: SettingsMenuSelection, edit_mode: bool },
    Saved { selected: SettingsMenuSelection, key: rltk::VirtualKeyCode },
    Cancel
}

fn get_key_name(gs: &State, menu_item: &SettingsMenuSelection) -> String {
    match menu_item {
        SettingsMenuSelection::MoveUp => get_key_as_str(&gs.settings.move_up),
        SettingsMenuSelection::MoveRight => get_key_as_str(&gs.settings.move_right),
        SettingsMenuSelection::MoveDown => get_key_as_str(&gs.settings.move_down),
        SettingsMenuSelection::MoveLeft => get_key_as_str(&gs.settings.move_left),
        SettingsMenuSelection::MoveUpRight => get_key_as_str(&gs.settings.move_up_right),
        SettingsMenuSelection::MoveDownRight => get_key_as_str(&gs.settings.move_down_right),
        SettingsMenuSelection::MoveDownLeft => get_key_as_str(&gs.settings.move_down_left),
        SettingsMenuSelection::MoveUpLeft => get_key_as_str(&gs.settings.move_up_left),
        SettingsMenuSelection::TakeStairs => get_key_as_str(&gs.settings.take_stairs),
        SettingsMenuSelection::PickupItem => get_key_as_str(&gs.settings.pickup_item),
        SettingsMenuSelection::Inventory => get_key_as_str(&gs.settings.inventory),
        SettingsMenuSelection::DropItem => get_key_as_str(&gs.settings.drop_item),
        SettingsMenuSelection::UnequipItem => get_key_as_str(&gs.settings.unequip_item),
        SettingsMenuSelection::MainMenu => get_key_as_str(&gs.settings.main_menu),
        SettingsMenuSelection::Wait => get_key_as_str(&gs.settings.wait),
        SettingsMenuSelection::Action1 => get_key_as_str(&gs.settings.action1),
        SettingsMenuSelection::Action2 => get_key_as_str(&gs.settings.action2),
        SettingsMenuSelection::Action3 => get_key_as_str(&gs.settings.action3),
        SettingsMenuSelection::Action4 => get_key_as_str(&gs.settings.action4),
        SettingsMenuSelection::Action5 => get_key_as_str(&gs.settings.action5),
        SettingsMenuSelection::Action6 => get_key_as_str(&gs.settings.action6),
        SettingsMenuSelection::Action7 => get_key_as_str(&gs.settings.action7),
        SettingsMenuSelection::Action8 => get_key_as_str(&gs.settings.action8),
        SettingsMenuSelection::Action9 => get_key_as_str(&gs.settings.action9),
        SettingsMenuSelection::Action10 => get_key_as_str(&gs.settings.action10),
        SettingsMenuSelection::Action11 => get_key_as_str(&gs.settings.action11),
        SettingsMenuSelection::Action12 => get_key_as_str(&gs.settings.action12),
    }
}

fn get_key_as_str(key: &Option<rltk::VirtualKeyCode>) -> String {
    match key {
        Some(k) => format!("{:?}", k),
        None => String::from("")
    }
}

pub fn settings_menu(gs: &mut State, ctx: &mut Rltk) -> SettingsMenuResult {
    let runstate = gs.ecs.fetch::<RunState>();
    let assets = gs.ecs.fetch::<RexAssets>();

    ctx.render_xp_sprite(&assets.menu, 0, 5);

    let (x_chars, y_chars) = ctx.get_char_size();
    let menu_items: Vec<SettingsMenuSelection>;
    {
        use SettingsMenuSelection::*;

        menu_items = vec![MoveUp, MoveRight, MoveDown, MoveLeft,
            MoveUpRight, MoveDownRight, MoveDownLeft, MoveUpLeft, TakeStairs,
            PickupItem, Inventory, DropItem, UnequipItem, MainMenu, Wait,
            Action1, Action2, Action3, Action4, Action5, Action6, Action7,
            Action8, Action9, Action10, Action11, Action12];
    }
    let menu_labels = vec!["Move Up", "Move Right", "Move Down", "Move Left",
        "Move Up-Right", "Move Down-Right", "Move Down-Left", "Move Up-Left",
        "Take Stairs", "Pick up Item", "Open Inventory", "Open Drop Menu",
        "Open Unequip Menu", "Return to Main Menu", "Wait", "Action1", "Action2",
        "Action3", "Action4", "Action5", "Action6", "Action7", "Action8", "Action9",
        "Action10", "Action11", "Action12",
    ];


    let width = 50;
    let height = 8 + menu_items.len();
    let top = (y_chars / 2) - (height / 2) as u32;

    ctx.draw_box_double((x_chars / 2) - (width / 2), top, width, height, RGB::named(rltk::WHEAT), RGB::named(rltk::BLACK));
    ctx.print_color_centered(top + 2, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), "Game Settings");
    ctx.print_color_centered(top + 3, RGB::named(rltk::GRAY), RGB::named(rltk::BLACK), "Use Up/Down Arrows and Enter");

    let mut y = top + 5;

    if let RunState::SettingsMenu { menu_selection: selection, edit_mode } = *runstate {

        let left = (x_chars / 2) - (width / 2) + 2;
        for (i, menu_item) in menu_items.iter().enumerate() {
            if selection == *menu_item {
                    ctx.print_color(left, y, RGB::named(rltk::MAGENTA), RGB::named(rltk::BLACK), menu_labels[i]);
                if edit_mode {
                    ctx.print_color(left + width - 12, y, RGB::named(rltk::YELLOW), RGB::named(rltk::GRAY), get_key_name(gs, menu_item));
                } else {
                    ctx.print_color(left + width - 12, y, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), get_key_name(gs, menu_item));
                }
            } else {
                ctx.print_color(left, y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), menu_labels[i]);
                ctx.print_color(left + width - 12, y, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), get_key_name(gs, menu_item));
            }
            y += 1;
        }

        if edit_mode {
            match ctx.key {
                None => return SettingsMenuResult::Selected { selected: selection, edit_mode: true },
                Some(key) => {
                    match key {
                        VirtualKeyCode::Escape => { return SettingsMenuResult::Selected { selected: selection, edit_mode: false } },
                        _ => {return SettingsMenuResult::Saved { selected: selection, key: key } }
                    }
                }
            }
        } else {
            match ctx.key {
                None => return SettingsMenuResult::Selected { selected: selection, edit_mode: false },
                Some(key) => {
                    match key {
                        VirtualKeyCode::Escape => { return SettingsMenuResult::Cancel}
                        VirtualKeyCode::Up => {
                            let newselection;
                            let pos = menu_items.iter().position(|&i| i == selection).unwrap();
                            if pos == 0 {
                                newselection = menu_items[menu_items.len()-1];
                            } else {
                                newselection = menu_items[pos - 1];
                            }
                            return SettingsMenuResult::Selected { selected: newselection, edit_mode: false };
                        }
                        VirtualKeyCode::Down => {
                            let newselection;
                            let pos = menu_items.iter().position(|&i| i == selection).unwrap();
                            if pos == menu_items.len() - 1 {
                                newselection = menu_items[0];
                            } else {
                                newselection = menu_items[pos + 1];
                            }
                            return SettingsMenuResult::Selected { selected: newselection, edit_mode: false };
                        }
                        VirtualKeyCode::Return => return SettingsMenuResult::Selected { selected: selection, edit_mode: true },
                        _ => return SettingsMenuResult::Selected { selected: selection, edit_mode: true }
                    }
                }
            }
        }
    }

    SettingsMenuResult::Selected { selected: SettingsMenuSelection::MoveUp, edit_mode: false }
}

pub fn remove_item_menu(gs: &mut State, ctx: &mut Rltk) -> (ItemMenuResult, Option<Entity>) {
    let player_entity = gs.ecs.fetch::<Entity>();
    let names = gs.ecs.read_storage::<Name>();
    let backpack = gs.ecs.read_storage::<Equipped>();
    let entities = gs.ecs.entities();

    let inventory = (&backpack, &names).join().filter(|item| item.0.owner == *player_entity);
    let count = inventory.count();

    let mut y = (25 - (count / 2)) as i32;
    ctx.draw_box(20, y - 2, 40, (count + 3) as i32, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK));
    ctx.print_color(23, y - 2, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), "Remove Which Item?");
    ctx.print_color(23, y + count as i32 + 1, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), "ESCAPE to cancel");

    let mut equippable: Vec<Entity> = Vec::new();
    let mut j = 0;
    for (entity, _pack, name) in (&entities, &backpack, &names).join().filter(|item| item.1.owner == *player_entity) {
        ctx.set(22, y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), rltk::to_cp437('('));
        ctx.set(23, y, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), 97 + j as rltk::FontCharType);
        ctx.set(24, y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), rltk::to_cp437(')'));

        ctx.print(26, y, &name.name.to_string());
        equippable.push(entity);
        y += 1;
        j += 1;
    }

    match ctx.key {
        None => (ItemMenuResult::NoResponse, None),
        Some(key) => {
            match key {
                VirtualKeyCode::Escape => { (ItemMenuResult::Cancel, None) }
                _ => {
                    let selection = rltk::letter_to_option(key);
                    if selection > -1 && selection < count as i32 {
                        return (ItemMenuResult::Selected, Some(equippable[selection as usize]));
                    }
                    (ItemMenuResult::NoResponse, None)
                }
            }
        }
    }
}

#[derive(PartialEq, Copy, Clone)]
pub enum GameOverResult { NoSelection, QuitToMenu }


pub fn game_over(ctx: &mut Rltk) -> GameOverResult {
    ctx.print_color_centered(20, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), "Your journey has ended!");
    ctx.print_color_centered(22, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), "One day, we'll tell you all about how you did");
    ctx.print_color_centered(23, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), "That day, sadly, is not in this chapter...");

    ctx.print_color_centered(25, RGB::named(rltk::MAGENTA), RGB::named(rltk::BLACK), "Press any key to return to the menu");

    match ctx.key {
        None => GameOverResult::NoSelection,
        Some(_) => GameOverResult::QuitToMenu
    }
}

pub fn draw_hollow_box(
    console: &mut Rltk,
    sx: i32,
    sy: i32,
    width: i32,
    height: i32,
    fg: RGB,
    bg: RGB
    )
{
    use rltk::to_cp437;

    console.set(sx, sy, fg, bg, to_cp437('┌'));
    console.set(sx + width, sy, fg, bg, to_cp437('┐'));
    console.set(sx, sy + height, fg, bg, to_cp437('└'));
    console.set(sx + width, sy + height, fg, bg, to_cp437('┘'));

    for x in sx + 1 .. sx + width {
        console.set(x, sy, fg, bg, to_cp437('─'));
        console.set(x, sy + height, fg, bg, to_cp437('─'));
    }

    for y in sy + 1 .. sy + height {
        console.set(sx, y, fg, bg, to_cp437('│'));
        console.set(sx + width, y, fg, bg, to_cp437('│'));
    }

}

fn draw_attribute(name: &str, attribute: &Attribute, y: i32, ctx: &mut Rltk) {
    let black = RGB::named(rltk::BLACK);
    let attr_gray: RGB = RGB::from_hex("#CCCCCC").expect("Oops");
    ctx.print_color(50, y, attr_gray, black, name);
    let color: RGB =
        if attribute.modifiers < 0 { RGB::from_f32(1.0, 0.0, 0.0) }
        else if attribute.modifiers == 0 { RGB::named(rltk::WHITE) }
        else { RGB::from_f32(0.0, 1.0, 0.0) };
    ctx.print_color(67, y, color, black, &format!("{}", attribute.base + attribute.modifiers));
    ctx.print_color(73, y, color, black, &format!("{}", attribute.bonus));
    if attribute.bonus > 0 { ctx.set(72, y, color, black, rltk::to_cp437('+')); }
}

struct Tooltip {
    lines: Vec<String>,
}

impl Tooltip {
    fn new() -> Tooltip {
        Tooltip { lines: Vec::new() }
    }

    fn add<S: ToString>(&mut self, line: S) {
        self.lines.push(line.to_string());
    }

    fn width(&self) -> i32 {
        let mut max = 0;
        for s in self.lines.iter() {
            if s.len() > max {
                max = s.len();
            }
        }
        max as i32 + 2i32
    }

    fn height(&self) -> i32 {
        self.lines.len() as i32 + 2i32
    }

    fn render(&self, ctx: &mut Rltk, x: i32, y: i32) {
        let box_gray: RGB = RGB::from_hex("#999999").expect("Oops");
        let light_gray: RGB = RGB::from_hex("#DDDDDD").expect("Oops");
        let white = RGB::named(rltk::WHITE);
        let black = RGB::named(rltk::BLACK);
        ctx.draw_box(x, y, self.width() - 1, self.height() - 1, white, box_gray);
        for (i, s) in self.lines.iter().enumerate() {
            let col = if i == 0 { white } else { light_gray };
            ctx.print_color(x + 1, y + i as i32 + 1, col, black, &s);
        }
    }
}
