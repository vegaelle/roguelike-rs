use std::collections::HashMap;
use rltk::{RGB, RandomNumberGenerator};
use specs::prelude::*;
use specs::saveload::{MarkedBuilder, SimpleMarker};
use super::random_table::RandomTable;
use super::components::*;
use super::map::Rect;
use crate::hunger_system::HUNGER_MAX_DURATION;
use super::{SHOW_MAPGEN_VISUALIZER, Map, TileType};
use super::raws::*;
use super::components::Renderable;
use super::{attr_bonus, player_hp_at_level, mana_at_level, DEBUG};

const MAX_MONSTERS: i32 = 4;

/// Spawns the player and returns their entity object
pub fn player(ecs: &mut World, player_x: i32, player_y: i32) -> Entity {

    let mut skills = Skills { skills: HashMap::new() };
    skills.skills.insert(Skill::Melee, 1);
    skills.skills.insert(Skill::Defense, 1);
    skills.skills.insert(Skill::Magic, 1);

    let mut base_stats = 11;

    if DEBUG {
        base_stats = 15;
    }

    let player_entity = ecs
        .create_entity()
        .with(Position { x: player_x, y: player_y })
        .with(Renderable {
            glyph: rltk::to_cp437('@'),
            fg: RGB::named(rltk::YELLOW),
            bg: RGB::named(rltk::BLACK),
            render_order: 0
        })
        .with(Player {})
        .with(Viewshed { visible_tiles: Vec::new(), range: 8, dirty: true })
        .with(Name { name: String::from("Player") })
        .with(HungerClock { state: HungerState::WellFed, duration: HUNGER_MAX_DURATION })
        .with(Attributes {
            might: Attribute { base: base_stats, modifiers: 0, bonus: attr_bonus(base_stats) },
            fitness: Attribute { base: base_stats, modifiers: 0, bonus: attr_bonus(base_stats) },
            quickness: Attribute { base: base_stats, modifiers: 0, bonus: attr_bonus(base_stats) },
            intelligence: Attribute { base: base_stats, modifiers: 0, bonus: attr_bonus(base_stats) },
        })
        .with(skills)
        .with(Pools {
            hit_points: Pool {
                current: player_hp_at_level(base_stats, 1),
                max: player_hp_at_level(base_stats, 1),
            },
            mana: Pool {
                current: mana_at_level(base_stats, 1),
                max: mana_at_level(base_stats, 1),
            },
            xp: 0,
            level: 1,
        })
        .marked::<SimpleMarker<SerializeMe>>()
        .build();

    // Starting equipment
    spawn_named_entity(&RAWS.lock().unwrap(), ecs, "Rusty Longsword", SpawnType::Equipped{ by: player_entity });
    spawn_named_entity(&RAWS.lock().unwrap(), ecs, "Stained Tunic", SpawnType::Equipped{ by: player_entity });
    spawn_named_entity(&RAWS.lock().unwrap(), ecs, "Torn Trousers", SpawnType::Equipped{ by: player_entity });
    spawn_named_entity(&RAWS.lock().unwrap(), ecs, "Old Boots", SpawnType::Equipped{ by: player_entity });
    spawn_named_entity(&RAWS.lock().unwrap(), ecs, "Dried Seitan Sausage", SpawnType::Carried{ by: player_entity });
    spawn_named_entity(&RAWS.lock().unwrap(), ecs, "Beer", SpawnType::Carried{ by: player_entity });

    if SHOW_MAPGEN_VISUALIZER {
        spawn_named_entity(&RAWS.lock().unwrap(), ecs, "Magic Mapping Scroll", SpawnType::Carried { by: player_entity });
        let mut names = ecs.write_storage::<Name>();
        let mut consumable = ecs.write_storage::<Consumable>();
        for (entity, _magic_mapper) in (&ecs.entities(), &ecs.read_storage::<MagicMapper>()).join() {
            names.remove(entity).expect("Unable to remove name");
            names.insert(entity, Name { name: String::from("_Debug Magic Scroll Mapper") }).expect("Unable to insert new name");
            consumable.remove(entity).expect("Unable to remove consumable");
        }
    }

    player_entity
}

/// Fills a room with stuff!
pub fn spawn_room(map: &Map, rng: &mut RandomNumberGenerator, room: &Rect, map_depth: i32, spawn_list: &mut Vec<(usize, String)>) {
    let mut possible_targets: Vec<usize> = Vec::new();
    { // Borrow scope - to keep access to the map separated
        for y in room.y1 + 1 .. room.y2 {
            for x in room.x1 + 1 .. room.x2 {
                let idx = map.xy_idx(x, y);
                if map.tiles[idx] == TileType::Floor {
                    possible_targets.push(idx);
                }
            }
        }
    }

    spawn_region(rng, &possible_targets, map_depth, spawn_list);
}

/// Fills a region with stuff!
pub fn spawn_region(rng: &mut RandomNumberGenerator, area: &[usize], map_depth: i32, spawn_list: &mut Vec<(usize, String)>) {
    let spawn_table = room_table(map_depth);
    let mut spawn_points: HashMap<usize, String> = HashMap::new();
    let mut areas: Vec<usize> = Vec::from(area);

    // Scope to keep the borrow checker happy
    {
        let num_spawns = i32::min(areas.len() as i32, rng.roll_dice(1, MAX_MONSTERS + 3));
        if num_spawns == 0 { return; }

        for _i in 0 .. num_spawns {
            let array_index = if areas.len() == 1 { 0usize } else {
                (rng.roll_dice(1, areas.len() as i32) - 1) as usize
            };
            let map_idx = areas[array_index];
            spawn_points.insert(map_idx, spawn_table.roll(rng));
            areas.remove(array_index);
        }
    }

    // Actually spawn the entities
    for spawn in spawn_points.iter() {
        spawn_list.push((*spawn.0, spawn.1.to_string()));
    }
}

/// Spawns a named entity (name in tuple.1) at the location in tuple.0
pub fn spawn_entity(ecs: &mut World, spawn: &(&usize, &String)) {
    let map = ecs.fetch::<Map>();
    let width = map.width as usize;
    let x = (*spawn.0 % width) as i32;
    let y = (*spawn.0 / width) as i32;
    std::mem::drop(map);

    let item_result = spawn_named_entity(&RAWS.lock().unwrap(), ecs, &spawn.1, SpawnType::AtPosition { x, y });
    if item_result.is_some() {
        return;
    }

    rltk::console::log(format!("WARNING: We don’t know how to spawn [{}]!", spawn.1));
}

fn room_table(map_depth: i32) -> RandomTable {
    get_spawn_table_for_depth(&RAWS.lock().unwrap(), map_depth)
    // RandomTable::new()
    //     .add("Goblin", 10)
    //     .add("Orc", 1 + map_depth)
    //     .add("Health Potion", 7)
    //     .add("Fireball Scroll", 2 + map_depth)
    //     .add("Confusion Scroll", 2 + map_depth)
    //     .add("Magic Missile Scroll", 4)
    //     .add("Dagger", 3)
    //     .add("Shield", 3)
    //     .add("Longsword", map_depth - 1)
    //     .add("Tower Shield", map_depth - 1)
    //     .add("Ration", 10)
    //     .add("Magic Mapping Scroll", 2)
    //     .add("Bear Trap", 2)
}
