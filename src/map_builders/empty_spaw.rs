use super::{MetaMapBuilder, BuilderMap};

#[derive(Debug)]
pub struct EmptySpawning {}

impl MetaMapBuilder for EmptySpawning {
    fn build_map(&mut self, _rng: &mut rltk::RandomNumberGenerator, build_data: &mut BuilderMap) {
        self.build(build_data);
    }
}

impl EmptySpawning {
    #[allow(dead_code)]
    pub fn new() -> Box<EmptySpawning> {
        Box::new(EmptySpawning {})
    }

    fn build(&mut self, build_data: &mut BuilderMap) {
        build_data.spawn_list = Vec::new();
    }
}
