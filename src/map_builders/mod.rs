use super::{Map, TileType, Rect, Position, World, spawner, SHOW_MAPGEN_VISUALIZER, map::tile_walkable};
use std::fmt::Debug;
use common::*;
// use room_based_spawner::RoomBasedSpawner;
use room_based_spawning_position::RoomBasedStartingPosition;
use room_based_stairs::RoomBasedStairs;
use area_starting_points::{AreaStartingPosition, XStart, YStart};
use cull_unreachable::CullUnreachable;
use voronoi_spawning::VoronoiSpawning;
use distant_exit::DistantExit;
use simple_map::SimpleMapBuilder;
use bsp_dungeon::BspDungeonBuilder;
use bsp_interior::BspInteriorBuilder;
use cellular_automata::CellularAutomataBuilder;
use drunkard::DrunkardsWalkBuilder;
use maze::MazeBuilder;
use dla::DLABuilder;
use voronoi::VoronoiBuilder;
use waveform_collapse::*;
use prefab_builder::*;
use room_exploder::*;
use room_corner_rounding::*;
use rooms_corridors_dogleg::*;
use rooms_corridors_bsp::*;
use rooms_corridors_nearest::*;
use rooms_corridors_lines::*;
use room_sorter::*;
use empty_spaw::EmptySpawning;
use room_draw::*;
use room_corridor_spawner::*;
use door_placement::*;
use town::town_builder;
use forest::forest_builder;

mod common;
mod room_based_spawner;
mod room_based_spawning_position;
mod room_based_stairs;
mod area_starting_points;
mod cull_unreachable;
mod voronoi_spawning;
mod distant_exit;
mod simple_map;
mod bsp_dungeon;
mod bsp_interior;
mod cellular_automata;
mod drunkard;
mod maze;
mod dla;
mod voronoi;
mod waveform_collapse;
mod prefab_builder;
mod room_exploder;
mod room_corner_rounding;
mod rooms_corridors_dogleg;
mod rooms_corridors_bsp;
mod rooms_corridors_nearest;
mod rooms_corridors_lines;
mod room_sorter;
mod empty_spaw;
mod room_draw;
mod room_corridor_spawner;
mod door_placement;
mod town;
mod forest;

#[cfg(debug_assertions)]
const DEBUG_MAP_BUILDERS: bool = true;

#[cfg(not(debug_assertions))]
const DEBUG_MAP_BUILDERS: bool = false;

pub trait InitialMapBuilder: Debug {
    fn build_map(&mut self, rng: &mut rltk::RandomNumberGenerator, build_data: &mut BuilderMap);
}

pub trait MetaMapBuilder: Debug  {
    fn build_map(&mut self, rng: &mut rltk::RandomNumberGenerator, build_data: &mut BuilderMap);
}

pub struct BuilderMap {
    pub spawn_list: Vec<(usize, String)>,
    pub map: Map,
    pub starting_position: Option<Position>,
    pub rooms: Option<Vec<Rect>>,
    pub corridors: Option<Vec<Vec<usize>>>,
    pub history: Vec<Map>,
    pub width: i32,
    pub height: i32,
    pub min_surface: f32,
}

impl BuilderMap {
    fn take_snapshot(&mut self) {
        if SHOW_MAPGEN_VISUALIZER {
            let mut snapshot = self.map.clone();
            for v in snapshot.revealed_tiles.iter_mut() {
                *v = true;
            }
            self.history.push(snapshot);
        }
    }
}

pub struct BuilderChain {
    starter: Option<Box<dyn InitialMapBuilder>>,
    builders: Vec<Box<dyn MetaMapBuilder>>,
    pub build_data: BuilderMap,
}

impl BuilderChain {
    pub fn new<S: ToString>(new_depth: i32, width: i32, height: i32, min_surface: f32, name: S) -> BuilderChain {
        BuilderChain {
            starter: None,
            builders: Vec::new(),
            build_data: BuilderMap {
                spawn_list: Vec::new(),
                map: Map::new(new_depth, width, height, name),
                starting_position: None,
                rooms: None,
                corridors: None,
                history: Vec::new(),
                width,
                height,
                min_surface,
            }
        }
    }

    pub fn start_with(&mut self, starter: Box<dyn InitialMapBuilder>) {
        match self.starter {
            None => {
                if DEBUG_MAP_BUILDERS {
                    println!("Starting builder with {:?}", starter);
                }
                self.starter = Some(starter);
            }
            Some(_) => panic!("You can only have one starting builder")
        };
    }

    pub fn with(&mut self, metabuilder: Box<dyn MetaMapBuilder>) {
        if DEBUG_MAP_BUILDERS {
            println!("Adding builder component {:?}", metabuilder);
        }
        self.builders.push(metabuilder);
    }

    pub fn build_map(&mut self, rng: &mut rltk::RandomNumberGenerator) {
        match &mut self.starter {
            None => panic!("Cannot run a map builder chain without a starting build system"),
            Some(starter) => {
                // Build the starting map
                starter.build_map(rng, &mut self.build_data);
            }
        }

        // Build additional layers in turn
        for metabuilder in self.builders.iter_mut() {
            metabuilder.build_map(rng, &mut self.build_data);
        }

        // Check the number of reachable tiles
        let mut num_floors = 0;
        for tile in self.build_data.map.tiles.iter() {
            if tile_walkable(*tile) { num_floors += 1; }
        }

        if num_floors < ((self.build_data.width * self.build_data.height) as f32 * self.build_data.min_surface) as usize {
            self.build_data.map = Map::new(self.build_data.map.depth, self.build_data.width, self.build_data.height, &self.build_data.map.name);
            self.build_data.spawn_list = Vec::new();
            self.build_map(rng);
        }
    }

    pub fn spawn_entities(&mut self, ecs: &mut World) {
        for entity in self.build_data.spawn_list.iter() {
            spawner::spawn_entity(ecs, &(&entity.0, &entity.1));
        }
    }
}

pub fn random_builder(new_depth: i32, rng: &mut rltk::RandomNumberGenerator, width: i32, height: i32) -> BuilderChain {
    // let mut builder_chain = BuilderChain::new(new_depth, width, height, 0.20);
    // builder_chain.start_with(BspDungeonBuilder::new());
    // builder_chain.with(RoomDrawer::new());
    // builder_chain.with(RoomSorter::new(room_sorter::RoomSort::TOPMOST));
    // builder_chain.with(StraightLineCorridors::new());
    // builder_chain.with(RoomBasedStartingPosition::new());
    // builder_chain.with(DistantExit::new());
    // builder_chain.with(RoomBasedSpawner::new());
    // builder_chain.with(DoorPlacement::new());
    // // builder_chain.with(CorridorSpawner::new());
    // // builder_chain.with(RoomBasedSpawner::new());
    // // builder_chain.with(AreaStartingPosition::new(XStart::LEFT, YStart::BOTTOM));
    // // builder_chain.with(RoomBasedStairs::new());
    // // builder_chain.with(VoronoiSpawning::new());
    // // builder_chain.with(WaveformCollapseBuilder::new());
    // // builder_chain.with(AreaStartingPosition::new(XStart::LEFT, YStart::TOP));
    // // builder_chain.with(CullUnreachable::new());
    // // builder_chain.with(DistantExit::new());
    // // builder_chain.with(EmptySpawning::new());
    // // builder_chain.with(VoronoiSpawning::new());
    // builder_chain.with(PrefabBuilder::vaults());

    // let mut builder_chain = BuilderChain::new(new_depth);
    // builder_chain.start_with(VoronoiBuilder::pythagoras());
    // builder_chain.with(CellularAutomataBuilder::new());
    // builder_chain.with(DoorPlacement::new());
    // builder_chain.with(AreaStartingPosition::new(XStart::CENTER, YStart::CENTER));
    // builder_chain.with(CullUnreachable::new());
    // builder_chain.with(VoronoiSpawning::new());
    // builder_chain.with(PrefabBuilder::sectional(prefab_builder::prefab_sections::UNDERGROUND_FORT));
    // builder_chain.with(DistantExit::new());

    // return builder_chain;

    let mut builder = BuilderChain::new(new_depth, width, height, 0.20, "New Map");

    let type_roll = rng.roll_dice(1, 2);
    match type_roll {
        1 => random_room_builer(rng, &mut builder),
        _ => random_shape_builder(rng, &mut builder)
    }

    if rng.roll_dice(1, 3) == 1 {
        builder.with(WaveformCollapseBuilder::new());

        // Now set the start to a random starting area
        let (start_x, start_y) = random_start_position(rng);
        builder.with(AreaStartingPosition::new(start_x, start_y));
        builder.with(CullUnreachable::new());

        builder.with(DistantExit::new());
        builder.with(EmptySpawning::new());
        builder.with(VoronoiSpawning::new());
    }

    if rng.roll_dice(1, 20) == 1 {
        builder.with(PrefabBuilder::sectional(prefab_builder::prefab_sections::UNDERGROUND_FORT));
    }

    builder.with(DoorPlacement::new());
    builder.with(PrefabBuilder::vaults());

    builder
}

fn random_start_position(rng: &mut rltk::RandomNumberGenerator) -> (XStart, YStart) {
    let x;
    let xroll = rng.roll_dice(1, 3);
    match xroll {
        1 => x = XStart::LEFT,
        2 => x = XStart::CENTER,
        _ => x = XStart::RIGHT
    }

    let y;
    let yroll = rng.roll_dice(1, 3);
    match yroll {
        1 => y = YStart::TOP,
        2 => y = YStart::CENTER,
        _ => y = YStart::BOTTOM
    }

    (x, y)
}

fn random_room_builer(rng: &mut rltk::RandomNumberGenerator, builder: &mut BuilderChain) {
    let build_roll = rng.roll_dice(1, 3);
    match build_roll {
        1 => builder.start_with(SimpleMapBuilder::new()),
        2 => builder.start_with(BspDungeonBuilder::new()),
        _ => builder.start_with(BspInteriorBuilder::new())
    }

    builder.with(RoomDrawer::new());


    // BSP Interior still makes holes in the walls
    if build_roll != 3 {
        // Sort by one of the 5 available algorithms
        let sort_roll = rng.roll_dice(1, 5);
        match sort_roll {
            1 => builder.with(RoomSorter::new(RoomSort::LEFTMOST)),
            2 => builder.with(RoomSorter::new(RoomSort::RIGHTMOST)),
            3 => builder.with(RoomSorter::new(RoomSort::TOPMOST)),
            4 => builder.with(RoomSorter::new(RoomSort::BOTTOMMOST)),
            _ => builder.with(RoomSorter::new(RoomSort::CENTRAL)),
        }

        let corridor_roll = rng.roll_dice(1, 4);
        match corridor_roll {
            1 => builder.with(DoglegCorridors::new()),
            2 => builder.with(NearestCorridors::new()),
            3 => builder.with(StraightLineCorridors::new()),
            _ => builder.with(BspCorridors::new())
        }

        let cspawn_roll = rng.roll_dice(1, 2);
        if cspawn_roll == 1 {
            builder.with(CorridorSpawner::new());
        }

        let modifier_roll = rng.roll_dice(1, 6);
        match modifier_roll {
            1 => builder.with(RoomExploder::new()),
            2 => builder.with(RoomCornerRounding::new()),
            _ => {}
        }
    }

    let start_roll = rng.roll_dice(1, 2);
    match start_roll {
        1 => builder.with(RoomBasedStartingPosition::new()),
        _ => {
            let (start_x, start_y) = random_start_position(rng);
            builder.with(AreaStartingPosition::new(start_x, start_y));
        }
    }

    let exit_roll = rng.roll_dice(1, 2);
    match exit_roll {
        1 => builder.with(RoomBasedStairs::new()),
        _ => builder.with(DistantExit::new())
    }

    let spawn_roll = rng.roll_dice(1, 2);
    match spawn_roll {
        // 1 => builder.with(RoomBasedSpawner::new()), // deactivated because with rooms
        //   modifications, it could spawn entities in walls
        _ => builder.with(VoronoiSpawning::new())
    }
}

fn random_shape_builder(rng: &mut rltk::RandomNumberGenerator, builder: &mut BuilderChain) {
    let builder_roll = rng.roll_dice(1, 15);
    match builder_roll {
        1 => builder.start_with(CellularAutomataBuilder::new()),
        2 => builder.start_with(DrunkardsWalkBuilder::open_area()),
        3 => builder.start_with(DrunkardsWalkBuilder::open_halls()),
        4 => builder.start_with(DrunkardsWalkBuilder::winding_passages()),
        5 => builder.start_with(DrunkardsWalkBuilder::fat_passages()),
        6 => builder.start_with(DrunkardsWalkBuilder::fearful_symmetry()),
        7 => builder.start_with(MazeBuilder::new()),
        8 => builder.start_with(DLABuilder::walk_inwards()),
        9 => builder.start_with(DLABuilder::central_attractor()),
        11 => builder.start_with(DLABuilder::insectoid()),
        12 => builder.start_with(VoronoiBuilder::pythagoras()),
        13 => builder.start_with(VoronoiBuilder::manhattan()),
        14 => builder.start_with(VoronoiBuilder::chebyshev()),
        _ => builder.start_with(PrefabBuilder::constant(prefab_builder::prefab_levels::WFC_POPULATED))
    }

    // Set the start to the center and cull
    builder.with(AreaStartingPosition::new(XStart::CENTER, YStart::CENTER));
    builder.with(CullUnreachable::new());

    // Now set the start to a random starting area
    let (start_x, start_y) = random_start_position(rng);
    builder.with(AreaStartingPosition::new(start_x, start_y));

    // Set up an exit and spawn mobs
    builder.with(VoronoiSpawning::new());
    builder.with(DistantExit::new());
}

pub fn level_builder(new_depth: i32, rng: &mut rltk::RandomNumberGenerator, width: i32, height: i32) -> BuilderChain {
    rltk::console::log(format!("Depth: {}", new_depth));
    match new_depth {
        1 => town_builder(new_depth, rng, width, height),
        2 => forest_builder(new_depth, rng, width, height),
        _ => random_builder(new_depth, rng, width, height)
    }
}
