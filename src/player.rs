use specs::prelude::*;
use std::cmp::{max, min};
use rltk::{Point, Rltk};
use super::{Position, World, Player, RunState, Viewshed, Map, State, Pools,
WantsToMelee, Item, gamelog::GameLog, WantsToPickupItem, TileType, Monster,
HungerState, HungerClock, EntityMoved, BlocksVisibility, Door, BlocksTile, Renderable,
Bystander, Vendor, Name};
use std::collections::HashMap;

pub fn try_move_player(delta_x: i32, delta_y: i32, ecs: &mut World) {
    let mut positions = ecs.write_storage::<Position>();
    let mut players = ecs.write_storage::<Player>();
    let mut viewsheds = ecs.write_storage::<Viewshed>();
    let pools = ecs.read_storage::<Pools>();
    let map = ecs.fetch::<Map>();
    let entities = ecs.entities();
    let mut wants_to_melee = ecs.write_storage::<WantsToMelee>();
    let mut entity_moved = ecs.write_storage::<EntityMoved>();
    let mut doors = ecs.write_storage::<Door>();
    let mut blocks_visibility = ecs.write_storage::<BlocksVisibility>();
    let mut blocks_movement = ecs.write_storage::<BlocksTile>();
    let mut renderables = ecs.write_storage::<Renderable>();
    let bystanders = ecs.read_storage::<Bystander>();
    let vendors = ecs.read_storage::<Vendor>();

    let mut swap_entities: Vec<(Entity, i32, i32)> = Vec::new();

    for (entity, _player, pos, viewshed) in (&entities, &mut players, &mut positions, &mut viewsheds).join() {
        if pos.x + delta_x < 1 || pos.x + delta_x > map.width - 1 ||
            pos.y + delta_y < 1 || pos.y + delta_y > map.height - 1 {
                return;
        }
        let destination_idx = map.xy_idx(pos.x + delta_x, pos.y + delta_y);

        for potential_target in map.tile_content[destination_idx].iter() {
            let bystander = bystanders.get(*potential_target);
            let vendor = vendors.get(*potential_target);
            if bystander.is_some() || vendor.is_some() {
                // Note that we want to move the bystander
                swap_entities.push((*potential_target, pos.x, pos.y));

                // Move the player
                pos.x = min(map.width - 1, max(0, pos.x + delta_x));
                pos.y = min(map.height - 1, max(0, pos.y + delta_y));
                entity_moved.insert(entity, EntityMoved {}).expect("Unable to insert marker");

                viewshed.dirty = true;
                let mut ppos = ecs.write_resource::<Point>();
                ppos.x = pos.x;
                ppos.y = pos.y;
            } else {
                let target = pools.get(*potential_target);
                if let Some(_target) = target {
                    // Attack it
                    wants_to_melee.insert(entity, WantsToMelee { target: *potential_target }).expect("Add target failed");
                    return;  // So we don’t move after attacking
                }
            }
            let door = doors.get_mut(*potential_target);
            if let Some(door) = door {
                door.open = true;
                blocks_visibility.remove(*potential_target);
                blocks_movement.remove(*potential_target);
                let glyph = renderables.get_mut(*potential_target).unwrap();
                glyph.glyph = rltk::to_cp437('/');
                viewshed.dirty = true;
            }
        }
        if !map.blocked[destination_idx] {
            pos.x = min(map.width - 1, max(0, pos.x + delta_x));
            pos.y = min(map.height - 1, max(0, pos.y + delta_y));
            let mut ppos = ecs.write_resource::<Point>();
            ppos.x = pos.x;
            ppos.y = pos.y;

            entity_moved.insert(entity, EntityMoved {}).expect("Unable to insert entity moved");

            viewshed.dirty = true;
        }
    }
    for m in swap_entities.iter() {
        let their_pos = positions.get_mut(m.0);
        if let Some(their_pos) = their_pos {
            their_pos.x = m.1;
            their_pos.y = m.2;
        }
    }
}

pub fn try_next_level(ecs: &mut World) -> bool {
    let player_pos = ecs.fetch::<Point>();
    let map = ecs.fetch::<Map>();
    let player_idx = map.xy_idx(player_pos.x, player_pos.y);
    if map.tiles[player_idx] == TileType::DownStairs {
        true
    } else {
        let mut gamelog = ecs.fetch_mut::<GameLog>();
        gamelog.entries.push(String::from("There is no way down from here."));
        false
    }
}

pub fn player_input(gs: &mut State, ctx: &mut Rltk) -> RunState {
    // Player movement
    match ctx.key {
        None => { return RunState::AwaitingInput },
        Some(key) => {
            let action_key = if gs.settings.action1 == Some(key) { Some(1) }
            else if gs.settings.action1 == Some(key) { Some(1) }
            else if gs.settings.action2 == Some(key) { Some(2) }
            else if gs.settings.action3 == Some(key) { Some(3) }
            else if gs.settings.action4 == Some(key) { Some(4) }
            else if gs.settings.action5 == Some(key) { Some(5) }
            else if gs.settings.action6 == Some(key) { Some(6) }
            else if gs.settings.action7 == Some(key) { Some(7) }
            else if gs.settings.action8 == Some(key) { Some(8) }
            else if gs.settings.action9 == Some(key) { Some(9) }
            else if gs.settings.action10 == Some(key) { Some(10) }
            else if gs.settings.action11 == Some(key) { Some(11) }
            else if gs.settings.action12 == Some(key) { Some(12) }
            else { None };
            if let Some(action_key) = action_key {
                return use_consumable_hotkey(gs, action_key - 1);
            }
            if gs.settings.move_up == Some(key) {
                try_move_player(0, -1, &mut gs.ecs)
            } else if gs.settings.move_right == Some(key) {
                try_move_player(1, 0, &mut gs.ecs)
            } else if gs.settings.move_down == Some(key) {
                try_move_player(0, 1, &mut gs.ecs)
            } else if gs.settings.move_left == Some(key) {
                try_move_player(-1, 0, &mut gs.ecs)
            } else if gs.settings.move_up_right == Some(key) {
                try_move_player(1, -1, &mut gs.ecs)
            } else if gs.settings.move_down_right == Some(key) {
                try_move_player(1, 1, &mut gs.ecs)
            } else if gs.settings.move_down_left == Some(key) {
                try_move_player(-1, 1, &mut gs.ecs)
            } else if gs.settings.move_up_left == Some(key) {
                try_move_player(-1, -1, &mut gs.ecs)
            } else if gs.settings.pickup_item == Some(key) {
                get_item(&mut gs.ecs)
            } else if gs.settings.inventory == Some(key) {
                return RunState::ShowInventory;
            } else if gs.settings.drop_item == Some(key) {
                return RunState::ShowDropItem;
            } else if gs.settings.take_stairs == Some(key) {
                if try_next_level(&mut gs.ecs) { return RunState::NextLevel; }
            } else if gs.settings.wait == Some(key) {
                return skip_turn(&mut gs.ecs);
            } else if gs.settings.unequip_item == Some(key) {
                return RunState::ShowRemoveItem;
            } else if gs.settings.main_menu == Some(key) {
                return RunState::SaveGame
            } else {
                return RunState::AwaitingInput;
            }
        }
    }
    RunState::PlayerTurn
}

fn get_item(ecs: &mut World) {
    let player_pos = ecs.fetch::<Point>();
    let player_entity = ecs.fetch::<Entity>();
    let entities = ecs.entities();
    let items = ecs.read_storage::<Item>();
    let positions = ecs.read_storage::<Position>();
    let mut gamelog = ecs.fetch_mut::<GameLog>();

    let mut target_item: Option<Entity> = None;
    for (item_entity, _item, position) in (&entities, &items, &positions).join() {
        if position.x == player_pos.x && position.y == player_pos.y {
            target_item = Some(item_entity);
        }
    }

    match target_item {
        None => gamelog.entries.push("There is nothing here to pick up.".to_string()),
        Some(item) => {
            let mut pickup = ecs.write_storage::<WantsToPickupItem>();
            pickup.insert(*player_entity, WantsToPickupItem { collected_by: *player_entity, item })
                .expect("Unable to insert want to pickup");
        }
    }
}

fn skip_turn(ecs: &mut World) -> RunState {
    let player_entity = ecs.fetch::<Entity>();
    let viewshed_components = ecs.read_storage::<Viewshed>();
    let monsters = ecs.read_storage::<Monster>();
    let hunger_clock = ecs.read_storage::<HungerClock>();

    let worldmap_resource = ecs.fetch::<Map>();

    let mut can_heal = true;
    let viewshed = viewshed_components.get(*player_entity).unwrap();
    for tile in viewshed.visible_tiles.iter() {
        let idx = worldmap_resource.xy_idx(tile.x, tile.y);
        for entity_id in worldmap_resource.tile_content[idx].iter() {
            let mob = monsters.get(*entity_id);
            match mob {
                None => {}
                Some(_) => { can_heal = false; }
            }
        }
    }

    let hunger = hunger_clock.get(*player_entity);
    if let Some(hunger) = hunger {
        if hunger.state == HungerState::Starving {
            can_heal = false;
        }
    }

    if can_heal {
        let mut health_components = ecs.write_storage::<Pools>();
        let pools = health_components.get_mut(*player_entity).unwrap();
        pools.hit_points.current = i32::min(pools.hit_points.current + 1, pools.hit_points.max);
    }

    RunState::PlayerTurn
}

fn use_consumable_hotkey(gs: &mut State, key: i32) -> RunState {
    use super::{Consumable, InBackpack, WantsToUseItem};

    let consumables = gs.ecs.read_storage::<Consumable>();
    let backpack = gs.ecs.read_storage::<InBackpack>();
    let player_entity = gs.ecs.fetch::<Entity>();
    let entities = gs.ecs.entities();
    let names = gs.ecs.read_storage::<Name>();

    let inventory = (&entities, &names, &consumables, &backpack).join().filter(|item| item.3.owner == *player_entity);
    let mut inventory_unique: HashMap<String, Entity> = HashMap::new();

    for (entity, name, _consumable, _in_backpack) in inventory {
        if !inventory_unique.contains_key(&name.name) {
            inventory_unique.insert(name.name.clone(), entity);
        }
    }
    let mut inventory_sorted = inventory_unique.iter().collect::<Vec<(&String, &Entity)>>();
    inventory_sorted.sort_by(|i, j| i.0.cmp(&j.0));

    if (key as usize) < inventory_sorted.len() {
        use crate::components::Ranged;
        if let Some(ranged) = gs.ecs.read_storage::<Ranged>().get(*inventory_sorted[key as usize].1) {
            return RunState::ShowTargeting{ range: ranged.range, item: *inventory_sorted[key as usize].1};
        }
        let mut intent = gs.ecs.write_storage::<WantsToUseItem>();
        intent.insert(
            *player_entity,
            WantsToUseItem{ item: *inventory_sorted[key as usize].1, target: None }
        ).expect("Unable to insert intent");
        return RunState::PlayerTurn;
    }
    RunState::PlayerTurn
}
