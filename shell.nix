let
  mozillaOverlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
  nixpkgs = import <nixpkgs> { overlays = [ mozillaOverlay ]; };
in

  with nixpkgs;

  mkShell {
    buildInputs = [
      alsaLib
      cmake
      freetype
      # latest.rustChannels.stable.rust
      (nixpkgs.rustChannelOf { date = "2021-05-10"; channel = "1.52.1"; }).rust
      expat
      openssl
      pkgconfig
      python3
      xlibs.libX11
      makeWrapper
    ];

    APPEND_LIBRARY_PATH = stdenv.lib.makeLibraryPath [
      xlibs.libXcursor
      xlibs.libXi
      xlibs.libXrandr
    ];

    shellHook = ''
      export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$APPEND_LIBRARY_PATH"
    '';
  postInstall = ''
    wrapProgram target/debug/roguelike-rs
      --suffix LD_LIBRARY_PATH : ${pkgs.xorg.libX11}/lib
      --suffix LD_LIBRARY_PATH : ${pkgs.xorg.libXcursor}/lib
      --suffix LD_LIBRARY_PATH : ${pkgs.xorg.libXrandr}/lib
      --suffix LD_LIBRARY_PATH : ${pkgs.xorg.libXi}/lib
      --suffix LD_LIBRARY_PATH : ${pkgs.libGL}/lib

  '';
  }

